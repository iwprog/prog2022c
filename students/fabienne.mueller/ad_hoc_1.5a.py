from turtle import*

reset()


laenge = numinput('Dreieck', 'Länge des Dreiecks')
farbe = textinput('Dreieck', 'Farbe')

pencolor(farbe)
pensize(5)
fillcolor('cyan')

begin_fill()
rt(30)
fd(laenge)
rt(120)
fd(laenge)
rt(120)
fd(laenge)
end_fill()

rt(60)
laenge = numinput('Dreieck', 'Länge')

fillcolor('lightgreen')

begin_fill()
forward(laenge)
left(120)
fd(laenge)
left(120)
fd(laenge)
end_fill()

laenge = numinput('Dreieck', 'Länge')
fillcolor('yellow')
begin_fill()

rt(120)
forward(laenge)
left(120)
fd(laenge)
left(120)
fd(laenge)
end_fill()


