from turtle import *
speed(10)
pensize(5)
pencolor("red")

laengeuno = numinput("1. Dreieck", "Länge des 1. Dreiecks?")
laengeduo = numinput("2. Dreieck", "Länge des 2. Dreiecks?")
laengetres = numinput("3. Dreieck", "Länge des 3. Dreiecks?")

fillcolor("cyan")
begin_fill()
right(30)
forward(laengeuno)
right(120)
forward(laengeuno)
right(120)
forward(laengeuno)
end_fill()

fillcolor("purple")
begin_fill()
forward(laengeduo)
right(120)
forward(laengeduo)
right(120)
forward(laengeduo)
right(120)
end_fill()

fillcolor("green")
begin_fill()
left(60)
forward(laengetres)
left(120)
forward(laengetres)
left(120)
forward(laengetres)
right(120)
end_fill()