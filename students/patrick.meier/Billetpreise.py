from turtle import *

alter = numinput("Bitte geben sie Ihr Alter ein", "Wie alt bist du?")
entf = numinput("Entfernung in km", "Wie lange ist die Strecke?")

def alterrabatt(alter):
    if alter < 6:
        c=0
    elif alter < 16:
        c=0.5  
    else:
        c=1
    return c
        
def streckenpreis(strecke):
    price = 0.25 * strecke + 2 
    return price

def billetpreis(alter, entf):
    price = streckenpreis(entf)
    factor = alterrabatt(alter)
    
    pricebillet = price * factor
    
    return pricebillet

print(billetpreis(alter, entf))

