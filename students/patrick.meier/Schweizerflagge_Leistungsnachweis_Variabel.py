from turtle import *

seiten = numinput("Seitenlänge", "Was ist die Seitenlänge der Schwiizer-Flagge?")
b= seiten*0.2
#Seitenverhältnis


def schwiizerflagge(a, b):
    speed(8)
    pencolor("white")
    fillcolor("red")
    begin_fill()
    forward(a)
    right(90)
    forward(a)
    right(90)
    forward(a)
    right(90)
    forward(a)
    end_fill()
    #Rotes Viereck als Hintergrund
    right(180)
    forward(a/2)
    left(90)
    penup()
    forward((a- (a*0.667))*0.5)
    pendown()
     #Startpunkt des Kreuzes in der Mitte.
    fillcolor("white")
    begin_fill()
    right(90)
    forward(b/2)
    left(90)
    forward(b*1.16666666666666666)
    right(90)
    forward(b*1.16666666666666666)
    left(90)
    forward(b)
    left(90)
    forward(b*1.16666666666666666)
    right(90)
    forward(b*1.16666666666666666)
    left(90)
    forward(b)
    left(90)
    forward(b*1.16666666666666666)
    right(90)
    forward(b*1.16666666666666666)
    left(90)
    forward(b)
    left(90)
    forward(b*1.16666666666666666)
    right(90)
    forward(b*1.16666666666666666)
    left(90)
    forward(b/2)
    end_fill()
     #Kreuz in der Mitte fertig
    penup()
    forward(20000)
    #dass "Turtle" weg ist
    


schwiizerflagge(seiten, b)
#input von Seitenlänge
