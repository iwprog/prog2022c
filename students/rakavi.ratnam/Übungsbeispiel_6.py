from turtle import *
def zeichne_rahmen(aussen, innen, rand, füllung):
    pencolor(rand)
    fillcolor(füllung)
    begin_fill()
    for i in range(4):
        forward(aussen)
        left(90)
    end_fill()
    penup()
    forward((aussen-innen)/2)
    left(90)
    forward((aussen-innen)/2)
    pendown()
    fillcolor("white")
    begin_fill()
    for i in range(4):
        forward(innen)
        right(90)
    end_fill()

zeichne_rahmen(100,25, "black", "white")
exitonclick()
