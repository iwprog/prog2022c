from json import loads, dumps
from csv import writer

#leere Liste
einkaufsliste = []

with open("einkaufsliste.json",encoding="utf8") as j:
    json_string=j.read()
    einkaufsliste = loads(json_string)

#Menü
while True:
    eingabe = input("Was möchten Sie machen?\n (1) Artikel hinzufügen\n (2) Artikel löschen\n (3) Artikel suchen\n (4) Einkaufsliste leeren\n (5) Einkaufsliste im CSV-Format exportieren\n (6) Gesamtbetrag Einkauf berechnen\n (0) Exit\n Auswahl: ")

    if eingabe == "0":
        with open("einkaufsliste.json","w",encoding="utf8") as g:
            json_string = dumps(einkaufsliste)
            g.write(json_string)
        break
    
    elif eingabe == "1":
        print("\n")
        artikel = input("Welchen Artikel möchten Sie hinzufügen? ")
        preis = input("Wie viel kostet der Artikel? ")
        menge = input("Wie viel Stück vom Artikel möchten Sie hinzufügen? ")
        einkaufsliste.append([artikel, preis, menge])
        print(einkaufsliste)
        print("\n")
        
    elif eingabe == "2":
        print("\n")
        print("Welchen Artikel möchten Sie löschen?", einkaufsliste)
        #Kopie der Einkaufsliste erstellen
        einkaufsliste_neu = einkaufsliste
        löschen = input("Auswahl: ")
        #Wert = ([Artikel, Preis, Menge], [Artikel, Preis, Menge], etc)
        for wert in einkaufsliste_neu:
            #Wert[0] = Artikel, Löschen = Artikel den wir eingegeben haben
            if wert[0] == löschen:
                einkaufsliste.remove(wert)
                
    elif eingabe == "3":
        print("\n")
        suche = input("Welchen Artikel möchten Sie suchen? ")
        ("\print")
        for wert in einkaufsliste:
            if suche in wert[0]:
                print("Der Artikel", suche, "wurde in der Einkaufsliste gefunden")
                print("\n")
                break
        #fehlt: Meldung wenn nicht gefunden
            
    elif eingabe == "4":
        #Fehler: löscht immer nur ein Artikel mit seinen Werten
        print("\n")
        leeren = input("Soll die Einkaufsliste wirklich gelöscht werden? (Ja / Nein) ")
        if leeren == "Ja":
            for wert in einkaufsliste:
                einkaufsliste.remove(wert)
                print("Die Einkaufsliste wurde geleert.")
                #print(einkaufsliste)
        else:
            print("Einkaufsliste leeren abgebrochen.")
            
    elif eingabe == "5":
        print("\n")
        with open("einkaufsliste.csv", "w", encoding = "utf8") as f:
            csv_writer = writer(f, delimiter = ";")
            csv_writer.writerow(["Artikel, Preis, Menge"])
            for wert in einkaufsliste:
                csv_writer.writerow([wert])
            print("Die Datei Einkaufsliste.csv wurde erfolgreich erstellt.")
                
    elif eingabe == "6":
        print("\n")
        summe = 0
        for wert in einkaufsliste:
            summe = summe + (float(wert[1]) * float(wert[2]))
        print("Das Total Ihrer Einkaufsliste beträgt CHF", summe)

#neu eingegebene Daten speichern
with open("berge.json", "w", encoding = "utf8") as f:
    json_string = dumps(einkaufsliste)
    f.write(json_string)
            
        
            
            
        

        