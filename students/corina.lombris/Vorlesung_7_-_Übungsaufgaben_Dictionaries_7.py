eingabetext = "David Mayer von der FHGR hat um 9:00 seine Wohnung in Chur verlassen."
blacklist = ["david", "mayer", "fhgr", "chur"]

worte = eingabetext.split()
ausgabe = ""

for i in worte:
    if i.lower() in blacklist:
        ausgabe = ausgabe + len(i) * "*" + " "
    else:
        ausgabe = ausgabe + i + " "

print("Eingabetext: ", eingabetext)
print("Blacklist: ", blacklist)
print("Ausgabe: ", ausgabe)
