#Aufgabe 1.1a

jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]
print(jahreszeiten)

#Aufgabe 1.1b
jahreszeiten.remove("Frühling")
print(jahreszeiten)

#Aufgabe 1.1c
jahreszeiten.append("Langas")
print(jahreszeiten)

#Aufgabe 1.1d
name = ""
liste = []
while name != "x":
    liste.append(name)
    name = input("Name: ")
    if name == "x":
        print("Liste: ", liste) 


