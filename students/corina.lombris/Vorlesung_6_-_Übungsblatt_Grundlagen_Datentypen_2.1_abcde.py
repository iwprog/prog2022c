#a
preisliste = {"Brot":3.2, "Milch":2.1,"Orangen":3.75, "Tomaten":2.2}

print(preisliste)

#b
preisliste["Milch"] = 2.05

print(preisliste)

#c
del preisliste["Brot"]
print(preisliste)

#d
preisliste["Tee"] = 4.2 
preisliste["Peanuts"] = 3.9
preisliste["Ketchup"] = 2.1
print(preisliste)

#e
liste = {}
def einkaufszettel(): 
        while input != "x":
            lebensmittel = input("Lebensmittel: ")
            preis = input("Preis: ")
            liste[lebensmittel] = preis
            if preis.isdigit():
                preis = float(preis)
            if lebensmittel == "x":
                del liste["x"]
                print("Dictionary: ", liste)
                break
einkaufszettel()








