from urllib.request import urlopen
from inscriptis import get_text

homepage = input("Fügen Sie hier Ihren Link ein: ")

with urlopen(homepage) as source:
    web_content = source.read().decode("utf8")
    text_content = get_text(web_content)
    print(text_content)
    
#fehlt: wenn kein "https" dann Text nehmen
#with open("text.txt", encoding = "utf8") as fileHandle:
    #text = fileHandle.read(): #Syntaxfehler? invalid syntax
    #print(text)
    
#Häufigkeiten Wörter zählen
häufigkeit_worte = {}
#Text in einzelne Wörter splitten
worte = web_content.split()
#geht jedes Wort durch und zählt die Anzahl
for wort in worte:
    if wort in häufigkeit_worte:
        häufigkeit_worte[wort] = häufigkeit_worte[wort] + 1
    else:
        häufigkeit_worte[wort] = 1

#CSV Datei erstellen
from csv import writer
with open("wort.csv", "w", encoding="utf8") as f:
    csv_writer = writer(f, delimiter=";")
    for wort in sorted(häufigkeit_worte):
        csv_writer.writerow([wort, häufigkeit_worte[wort]])
        
#Häufigkeiten Buchstaben zählen
häufigkeit_buchstaben = {}

#nimmt Inhalt aus der Homepage
buchstaben = web_content

#geht jeden Buchstaben durch und zählt die Anzahl
for wort in buchstaben:
    if wort in häufigkeit_buchstaben:
        häufigkeit_buchstaben[wort] = häufigkeit_buchstaben[wort] + 1
    else:
        häufigkeit_buchstaben[wort] = 1

#CSV Datei erstellen
from csv import writer
with open("buchstaben.csv", "w", encoding="utf8") as f:
    csv_writer = writer(f, delimiter=";")
    for wort in sorted(häufigkeit_buchstaben):
        csv_writer.writerow([wort, häufigkeit_buchstaben[wort]])