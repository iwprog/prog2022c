#Variante 1 Rabatt wird direkt abgezogen --> Ausgabe Summe - Rabatt
summe = 0
preis = 0
nummer = 1
while preis != "x":
       preis = input("Geben Sie den " + str(nummer) + ". Preis ein: ")
       if preis.isdigit():
           preis = float(preis)
           nummer = nummer + 1
           summe = summe + preis
            
if summe < 100:
    print("Kein Rabatt, Gesamtpreis = ", summe, "CHF")
    
elif summe < 1000:
    rabatt5 = summe * 0.95
    print("5% Rabatt, Gesamtpreis = ", rabatt5, "CHF")
    
else:
    rabatt10 = summe * 0.9
    print("10% Rabatt, Gesamtpreis = ", rabatt10, "CHF")
    
#Variante 2 Gesamtpreis ohne Abzug Rabatt --> Ausgabe Summe
summe = 0
preis = 0
nummer = 1
while preis != "x":
       preis = input("Geben Sie den " + str(nummer) + ". Preis ein: ")
       if preis.isdigit():
           preis = float(preis)
           nummer = nummer + 1
           summe = summe + preis
            
if summe < 100:
    print("Kein Rabatt, Gesamtpreis = ", summe, "CHF")
    
elif summe < 1000:
    rabatt5 = summe * 0.95
    print("5% Rabatt, Gesamtpreis = ", summe, "CHF")
    
else:
    rabatt10 = summe * 0.9
    print("10% Rabatt, Gesamtpreis = ", summe, "CHF")
    