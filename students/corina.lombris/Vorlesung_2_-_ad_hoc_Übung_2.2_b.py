#Variante 1
#Import Bibliothek Turtle
from turtle import *

#Tastatureingaben
laenge1 = numinput ("erstes Dreieck", "Bitte geben Sie die Seitenlänge an")
laenge2 = numinput ("zweites Dreieck", "Bitte geben Sie die Seitenlänge an")
laenge3 = numinput ("drittes Dreieck", "Bitte geben Sie die Seitenlänge an")

#Grundeinstellung Stift
pensize(3)
pencolor("red")
speed(2)
hideturtle()

#Zeichnung Dreieck
def dreieck(seitenlaenge, fuellfarbe):
    fillcolor(fuellfarbe)
    begin_fill()
    right(90)
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    left(90)
    end_fill()
    
dreieck(laenge1, "cyan")
dreieck(laenge2, "yellow")
dreieck(laenge3, "lime")

exitonclick()

#clear()

##Variante 2
##Import Bibliothek Turtle
from turtle import *

##Tastatureingaben

##Grundeinstellung Stift
pensize(3)
pencolor("red")
speed(2)
hideturtle()

##Zeichnung Dreieck
def dreieck(seitenlaenge, fuellfarbe):
    fillcolor(fuellfarbe)
    begin_fill()
    right(90)
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    left(90)
    end_fill()
 
laenge1 = numinput ("erstes Dreieck", "Bitte geben Sie die Seitenlänge an")
dreieck(laenge1, "cyan")
laenge2 = numinput ("zweites Dreieck", "Bitte geben Sie die Seitenlänge an")
dreieck(laenge2, "yellow")
laenge3 = numinput ("drittes Dreieck", "Bitte geben Sie die Seitenlänge an")
dreieck(laenge3, "lime")

exitonclick()

#Corina Lombris, ISc20tzC