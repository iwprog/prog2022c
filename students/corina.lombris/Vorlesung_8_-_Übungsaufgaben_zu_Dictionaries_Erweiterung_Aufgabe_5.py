#FEHLT: wenn "berge.json" existiert, sollen die berge aus der datei eingelesen werden. --> FileExists?

from json import loads, dumps

#neues Dictionary
berge={}

#Daten einlesen
with open("berge.json", encoding = "utf8") as f:
    json_string = f.read()
    berge = loads(json_string)

#drei Wiederholungen
for i in range(3):
    berg = input(str(i + 1) + ". Berg: ")
    hoehe=input(str(i + 1) + ". Höhe: ")
    print("\n")
    #Werte dem Dictionary hinzufügen
    berge[berg] = hoehe
    
#liste erstellen die alphabetisch sortiert wird
bergliste = sorted(berge)

#neu eingegebene Daten speichern
with open("berge.json", "w", encoding = "utf8") as f:
    json_string = dumps(berge)
    f.write(json_string)
    
for i in bergliste:
    print(i, "ist" ,berge[i] ,"m (" + str(int(round((int(berge[i]) * 3.28), 0))) + " ft) hoch.")
    

