#Variante 1
from math import pi

def volumen(radius, hoehe):
    return pi * (radius**2) * hoehe
    
print("Das Volumen beträgt:", volumen(2,2))

##Variante 2
from math import pi
from turtle import *

def volumen(radius, hoehe):
    return pi * (radius**2) * hoehe

radius = numinput("Abfrage", "Welchen Radius hat der Zylinder?")
hoehe = numinput ("Abfrage", "Welche Höhe hat der Zylinder?")
    
print("Das Volumen beträgt:", volumen(radius, hoehe))