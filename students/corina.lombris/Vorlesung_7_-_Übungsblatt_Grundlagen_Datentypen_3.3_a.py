#Telefonbuch aus Übung 3.1
telefonbuch = []
person1 = {"Vorname":"Ana", "Nachname":"Skupch", "Phone":123}
person2 = {"Vorname":"Tim", "Nachname":"Kurz", "Phone":732}
person3 = {"Vorname":"Julia", "Nachname":"Lang", "Phone":912}
telefonbuch.append(person1)
telefonbuch.append(person2)
telefonbuch.append(person3)

telefonbuch_neu=[]
for person in telefonbuch:
    if "1" not in str(person["Phone"]): #Wert für Schlüssel "Phone"
        telefonbuch_neu.append(person)

print(telefonbuch_neu)
