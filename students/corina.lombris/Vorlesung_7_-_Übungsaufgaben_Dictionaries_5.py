#neues Dictionary
berge={}

#drei Wiederholungen
for i in range(3):
    berg = input(str(i + 1) + ". Berg: ")
    hoehe=input(str(i + 1) + ". Höhe: ")
    print("\n")
    #Werte dem Dictionary hinzufügen
    berge[berg] = hoehe
    
#liste erstellen die alphabetisch sortiert wird
bergliste = sorted(berge)

for i in bergliste:
    print(i, "ist" ,berge[i] ,"m (" + str(int(round((int(berge[i]) * 3.28), 0))) + " ft) hoch.")
