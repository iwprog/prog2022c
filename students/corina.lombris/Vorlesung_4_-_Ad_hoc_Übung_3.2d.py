def replace(text, position, einfügetext):
    text = text[:position-1] + einfügetext + text[position-1:]
    #text[:position-1] gibt "Das ist eine einfache" aus --> 6 Zeichen - 1
    #einfügetext --> fügt unser Ersatzwort "Python" ein
    #text[position-1:] gibt "Sequenz." aus --> 
    
    return text

print(replace("Das ist eine einfache Sequenz.", 22, " Python"))