from turtle import *

def guten_morgen(name):
    return("Guten Morgen " + name + "!")

name = textinput("Name", "Wie heissen Sie?")

gruss = guten_morgen(name)
print(gruss)

exitonclick()
