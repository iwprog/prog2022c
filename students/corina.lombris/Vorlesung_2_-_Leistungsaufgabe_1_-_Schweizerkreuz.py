#Import Bibliothek Turtle
from turtle import *

#Tastatureingabe
laenge = numinput ("Länge des Vierecks", "Geben Sie die länge des Vierecks an")

#Grundeinstellung Stift Flagge
hideturtle() #Zeiger verstecken
pensize(5)
pencolor("red")
speed(10)

#Viereck
laenge = laenge #immer die Länge, die man manuell eingibt

#Stift zentrieren
penup()
rt(90)
fd(int(laenge/2)) #immer die Hälfte von der Länge, die man manuell eingibt, int() falls die manuelle eingabe nicht durch 2 teilbar ist
pendown()

#Zeichnung Viereck
fillcolor("red")
begin_fill()
lt(90)
fd(int(laenge/2))
lt(90)
fd(laenge)
lt(90)
fd(laenge)
lt(90)
fd(laenge)
lt(90)
fd(int(laenge/2))
end_fill()

#Grundeinstellung Stift Kreuz
pencolor("white")
fillcolor("white")

#Stift zentrieren
penup()
lt(90)
fd(int(laenge/5))
rt(90)
pendown()

#Zeichnung Kreuz
begin_fill()
fd(int(laenge/10))
lt(90)
fd(int(laenge/5))
rt(90)
fd(int(laenge/5))
lt(90)
fd(int(laenge/5))
lt(90)
fd(int(laenge/5))
rt(90)
fd(int(laenge/5))
lt(90)
fd(int(laenge/5))
lt(90)
fd(int(laenge/5))
rt(90)
fd(int(laenge/5))
lt(90)
fd(int(laenge/5))
lt(90)
fd(int(laenge/5))
rt(90)
fd(int(laenge/5))
lt(90)
fd(int(laenge/10))
end_fill()

exitonclick()



