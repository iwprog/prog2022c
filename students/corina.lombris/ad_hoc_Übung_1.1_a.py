#Variante 1
print("Corina", "Lombris", "Emmaweg", 2, 7000, "Chur")

#Zeilenumbruch
print("\n")

#Variante 2
print("Corina", "Lombris")
print("Emmaweg", 2)
print(7000, "Chur")

#Zeilenumbruch
print("\n")

#Variante 3
name="Corina"
vorname="Lombris"
strasse="Emmaweg"
hausnr="2"
plz="7000"
wohnort="Chur"

print(name, vorname)
print(strasse, hausnr)
print(plz, wohnort)

#Corina Lombris, ISc20tzC