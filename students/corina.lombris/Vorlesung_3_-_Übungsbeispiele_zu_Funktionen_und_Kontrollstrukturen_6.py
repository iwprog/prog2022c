from turtle import *
#speed (1)

def zeichne_rahmen(laenge, breite, rahmen, fuellfarbe):
    pencolor(rahmen)
    fillcolor(fuellfarbe)
    begin_fill()
    for i in range(4):
        fd(laenge)
        lt(90)
        
    for i in range(1):
        penup()
        fd (int((laenge - breite) / 2))
        lt(90)
        fd (int((laenge - breite) / 2))
        rt(90)
        pendown()

    for i in range(4):
        fd(breite)
        lt(90)
        
    end_fill()
        
        
zeichne_rahmen(100, 25, "black", "white")

exitonclick()
        

