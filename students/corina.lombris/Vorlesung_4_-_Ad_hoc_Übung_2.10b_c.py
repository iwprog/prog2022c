from turtle import *
#Grundeinstellungen Stift
pensize(2)
speed(10)
pencolor("blue")
fillcolor("blue")
shape("turtle")

#Stift in Position bringen, obere linke Ecke
penup()
back(1100)
lt(90)
fd(400)
rt(90)
pendown()

#Input wie viele Dreiecke pro Zeile wir haben möchten.
anzahl_pro_zeile = int(numinput("Anzahl Dreiecke", "Wie viele Dreiecke pro Zeile sollen gezeichnet werden?"))

#Input wie viele Zeilen wir haben möchten.
anzahl_zeilen = int(numinput("Anzahl Zeilen", "Wie viele Zeilen wollen Sie?"))

#i = Anzahl der Zeilen --> wie Input
for i in range(anzahl_zeilen):
    begin_fill()
    #Anzahl der Dreiecke die wir möchten --> wie Input
    for ii in range(anzahl_pro_zeile):          
        #Abstand zwischen Dreiecke
        for a in range(1):
            penup()
            fd(150)
            pendown()
            #Zeichnung des Dreiecks
            for b in range(3):
                fd(100)
                lt(120)
    #Positionierung Stift in nächster Zeile
    penup()            
    back(anzahl_pro_zeile * 150)
    rt(90)
    fd(150)
    lt(90)
    pendown()
    end_fill()
        
exitonclick()