l_input = "Der Tag begann sehr gut ! Der Morgen war schön ."

def wörter_zählen(l_input):
    dictionary = {}
    worte = l_input.split()
    
    for eintrag in worte:
        if eintrag in dictionary:
            dictionary[eintrag] = dictionary[eintrag] + 1
        else:
            dictionary[eintrag] = 1
            
    return dictionary

print(wörter_zählen(l_input))