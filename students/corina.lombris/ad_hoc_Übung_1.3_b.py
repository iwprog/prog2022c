#Import Bibliothek Turtle
from turtle import *

#Grundeinstellungen Stift
pensize(5)
pencolor("red")
speed(10)

#Zeichnung Quadrat türkis
fillcolor("cyan")
begin_fill()
rt(50)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
end_fill()

#Zeichnung Quadrat gelb
fillcolor("yellow")
begin_fill()
lt(110)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
end_fill()

#Zeichnung Quadrat pink
fillcolor("magenta")
begin_fill()
rt(170)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
end_fill()

#Zeichnung Quadrat blau
fillcolor("blue")
begin_fill()
rt(160)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
rt(90)
fd(100)
end_fill()

#Zeichnung Quadrat grün
fillcolor("lime")
begin_fill()
lt(105)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
end_fill()

exitonclick()

#Corina Lombris, ISc20tzC