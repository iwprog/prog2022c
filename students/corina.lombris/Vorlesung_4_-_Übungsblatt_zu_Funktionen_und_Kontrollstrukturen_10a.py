#10a

#Funktionen
# Programmübersicht
def programmuebersicht():
    print(50 * "=")
    print("Programmübersicht:" "\n 1 ... Preis für eine Fahrkarte berechnen" "\n 2 ... Maximale Herzfrequenz berechnen \n" "\n 0 ... Programm beenden")
    print(50 * "=")
    
# 1. Preis Fahrkarte
def get_billet_preis(alter, entfernung):
    grundpreis = 2
    
    if alter < 6:
        preis = 0
        
    elif alter < 16:
        preis = (((entfernung * 0.25) + grundpreis) / 2)
        
    else:
        preis = ((entfernung * 0.25) + grundpreis)
        
    return(int(preis))

#2. Berechnung Herzfrequenz
def berechne_herzfrequenz(age):
    herzfrequenz = 220 - age
    return int(herzfrequenz)
        
#Abfrage
programmuebersicht()

auswahl_option = ""    
auswahl_option = input("Gewählte Option: ")
print("\n")

#Option 1
if auswahl_option == "1":
    print("Preis für Fahrkarte berechnen")
    print(50 * "-")
    
    alter = input("Bitte geben Sie ihr Alter ein: ")
    entfernung = input("Wie viele km möchten Sie fahren? ")
    
    if alter.isdigit():
        alter = int(alter)
        
    if entfernung.isdigit():
        entfernung = int(entfernung)
    
    print("\n")
    print("Die Fahrkarte kostet", get_billet_preis(alter, entfernung), "CHF")
    print("\n")
    programmuebersicht()
    auswahl_option = input("Gewählte Option: ")

#Option 2 
if auswahl_option == "2":
    print("Maximale Herzfrequenz berechnen")
    print(50 * "-")
    
    age = input("Bitte geben Sie ihr Alter ein: ")
    
    if age.isdigit():
        age = int(age)
        
    print("\n")
    print("Die maximale Herzfrequenz beträgt:", berechne_herzfrequenz(age))
    print("\n")
    programmuebersicht()
    auswahl_option = input("Gewählte Option: ")

#Ungültige Option
elif auswahl_option > "2":
    print("Ungültige Option!")
    print("\n")
    programmuebersicht()
    auswahl_option = input("Gewählte Option: ")
    
