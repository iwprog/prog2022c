#Import Bibliothek Turtle
from turtle import *

#Tastatureingaben
laenge = numinput ("Dreieck", "Geben Sie die länge des Dreiecks an")

#Grundeinstellung Stift
pensize(5)
pencolor("red")
speed(10)

#Zeichnung Dreieck grün
fillcolor("lime")
begin_fill()
lt(90)
fd(laenge)
rt(120)
fd(laenge)
rt(120)
fd(laenge)
end_fill()

#Verdoppelung der Länge
laenge = laenge * 2
#Zeichnung Dreieck türkis
fillcolor("cyan")
begin_fill()
lt(120)
fd(laenge)
rt(120)
fd(laenge)
rt(120)
fd(laenge)
end_fill()

#Verdoppelung der länge
laenge = laenge * 2
#Zeichnung Dreieck gelb
fillcolor("yellow")
begin_fill()
lt(120)
fd(laenge)
rt(120)
fd(laenge)
rt(120)
fd(laenge)
end_fill()

exitonclick()

#Corina Lombris, ISc20tzC
