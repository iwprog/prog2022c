#Import Bibliothek Turtle
from turtle import *

#Grundeinstellungen Stift
pensize(5)
speed(5)

#Zeichnung rotes Dreieck
pencolor("red")
fd(100)
lt(120)
fd(100)
lt(120)
fd(100)

#Zeichnung grünes Dreieck
pencolor("lime")
fd(100)
lt(120)
fd(100)
lt(120)
fd(100)

#Zeichnung blaues Dreieck
pencolor("blue")
fd(100)
lt(120)
fd(100)
lt(120)
fd(100)

exitonclick()

#Corina Lombris, ISc20tzC





