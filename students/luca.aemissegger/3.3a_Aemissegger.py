# Entfernen Sie alle Datensätze aus dem Telefonbuch, bei denen eine 1 in
# der Telefonnummer vorkommt.
daten1={"Vorname":"Ana","Nachname":"Skupch","Telephonnr":123}
daten2={"Vorname":"Tim","Nachname":"Kurz","Telephonnr":732}
daten3={"Vorname":"Julia","Nachname":"Lang","Telephonnr":912}
telefonbuch=[]
telefonbuch.append(daten1)
telefonbuch.append(daten2)
telefonbuch.append(daten3)

gefiltertnach1 = [x for x in telefonbuch if "2" not in str(x["Telephonnr"])]
print(gefiltertnach1)
