# Bitte verwenden Sie das Telefonbuch aus dem obigen Beispiel für die folgen-
# den Übungen:
# a. Schreiben Sie eine Funktion datensatz_ausgeben, welche den ersten Da-
# tensatz aus obigen Beispiel wie folgt ausgibt:
# Vorname : Ana
# Nachname: Skupch
# Phone : 123
#erstellen des Datensatzes für die übernahme ins telefonbuch
daten1 = {"Vorname":"Nina", "Nachname": "Lang","Phone":8952545}
daten2={"Vorname":"Luca","Nachname":"Breit","Phone":8465165}
daten3={"Vorname":"Boris","Nachname":"Krumm","Phone":846514}

telefonbuch= []#leeres telefonbuch erstellen
#einfügen der daten ins telefonbuch
telefonbuch.append(daten2)
telefonbuch.append(daten3)
telefonbuch.append(daten1)
#erstellen der Funktion 
def datensatz_ausgeben(telefonbuch):
    ausgabe=""
    for i,daten in enumerate(telefonbuch):#damit es als liste rauskommt
        ausgabe=ausgabe+"\n\nVorname: "+telefonbuch[i]["Vorname"]+"\nNachname: "+telefonbuch[i]["Nachname"]+"\nPhone: "+str(telefonbuch[i]["Phone"])
    return ausgabe
print(datensatz_ausgeben(telefonbuch))
