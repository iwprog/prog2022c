from turtle import *

rahmenaussen = input ("Bitte geben Sie die Aussenlänge ein ")#entgegennahme des Alter
rahmenaussen = int(rahmenaussen) #umwandel des Alters in integer für die Berechnung
rahmeninnen = input ("Bitte geben Sie die Innenlänge ein ")#entgegennahme des Alter
rahmeninnen = int(rahmeninnen) #umwandel des Alters in integer für die Berechnung
farbeaussen = input ("Bitte geben Sie die Rahmenfarbe ein ")#entgegennahme des Alter
farbeinnen = input ("Bitte geben Sie die Innenfarbe  ein ")#entgegennahme des Alter


def zeichne_rahmen(rahmenaussen, rahmeninnen, farbeaussen, farbeinnen):
    for i in range(2):
        if i == 0:
            fillcolor(farbeaussen)
            begin_fill()
            fd(rahmenaussen)
            left(90)
            fd(rahmenaussen)
            left(90)
            fd(rahmenaussen)
            left(90)
            fd(rahmenaussen)
            end_fill()
        else:
            fillcolor(farbeinnen)
            
            penup()
            left(90)
            fd((rahmenaussen-rahmeninnen)/2)
            left(90)
            fd((rahmenaussen-rahmeninnen)/2)
            right(90)
            pendown()
            begin_fill()
            fd(rahmeninnen)
            left(90)
            fd(rahmeninnen)
            left(90)
            fd(rahmeninnen)
            left(90)
            fd(rahmeninnen)
            end_fill()
            
    
zeichne_rahmen(rahmenaussen, rahmeninnen, farbeaussen, farbeinnen)
