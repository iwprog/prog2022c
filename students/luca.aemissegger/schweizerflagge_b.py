#Schweizer Flagge a.)
from turtle import *
#allgemeine Eingaben 
hideturtle()
pencolor("red")
#roterhintergrund erstellen
def roterhintergrund(seitenlaenge):
    fillcolor("red")
    begin_fill()
    #befehl 4 mal die Kanten zu zeichnen
    for i in range(4):
        forward(seitenlaenge)
        left(90)
    end_fill()



#Eingabe bezüglich der Seiten der Fahne des Nutzers
seitenlaenge=numinput("Quadrat","Seitenlänge der Schweizer Flagge")
#roter hintergrund erstellen
roterhintergrund(seitenlaenge)