from turtle import *

def dreieck():
    begin_fill()
    fd(length)
    left(120)
    fd(length)
    left(120)
    fd(length)
    end_fill()

length = numinput("Seitenlänge","Eingeben")
farbe = textinput("Farbe","Eingeben")
pensize(5)
pencolor('red')

fillcolor(farbe)
left(30)
dreieck()

#start yellow
length = numinput("Seitenlänge","Eingeben")
farbe = textinput("Farbe","Eingeben")
fillcolor(farbe)
right(120)
dreieck()

#start blue
length = numinput("Seitenlänge","Eingeben")
farbe = textinput("Farbe","Eingeben")
fillcolor(farbe)
right(120)
dreieck()

exitonclick()