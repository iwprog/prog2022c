from csv import writer, reader
from json import loads, dumps



einkaufsliste = []

try:
    with open ("Einkaufsliste.json", encoding="utf8") as e:
        print("Liste geöffnet.")
        json_string=e.read()
        einkaufsliste=loads(json_string)

except FileNotFoundError:
    print("Erstelle neue Datei.")

while True:
    aktion=input("Wählen Sie eine der folgenden Aktionen:\n1...Artikel hinzufügen\n2...Artikel löschen\n3...Artikel Suchen\n4...Einkaufsliste leeren\n5...Einkaufsliste als cvs exportieren\n6...Gesamtbetrag der Einkaufsliste berechenen\n0...Exit\nAuswahl: ")
    if aktion == "0":
        with open("Einkaufsliste.json", "w", encoding="utf8") as o:
            json_string=dumps(einkaufsliste)
            o.write(json_string)
        break
    
    elif aktion == "1":
        name=input("Geben Sie den Namen des Artikels ein: ")
        preis=input("Geben Sie den Preis des Artikels ein: ")
        menge=input("Geben Sie die Menge des Artikels ein:")
        for i in einkaufsliste:
            if name.lower() in i[0].lower():
                i[2] = int(i[2]) + int(menge)
        
        einkaufsliste.append([name,menge,preis])
        
    elif aktion == "2":
        entfernen=input("Geben Sie den artikel ein, den Sie entfernen möchten: ")
        liste2=einkaufsliste
        for i in liste2:
            if i[0].lower() == entfernen.lower():
                einkaufsliste.remove(i)
                print("Der Artikel wurde gelöscht.")
                
    elif aktion == "3":
        suche=input("Geben Sie den Artikel ein, den Sie suchen möchten: ")
        ergebniss = ""
        for i in einkaufsliste:
            if suche.lower() in i[0].lower():
                ergebniss = ergebniss+i[0]+"; "
        print("Es wurden: ", ergebniss[:-2], "Gefunden")
                
    elif aktion == "4":
        einkaufsliste = []
        print("Die Einkaufsliste wurde gelöscht!")
        
    elif aktion == "5":
        with open("Einkaufsliste.csv", "w", encoding="utf8") as c:
            csv_writer=writer(c,delimiter=";")
            csv_writer.writerow(["Produkt", "Menge", "Preis"])
            for i in einkaufsliste:
                csv_writer.writerow(i)
            print("Liste wurde als CSV exportiert.")
            
    elif aktion == "6":
        sum = 0
        for i in einkaufsliste:
            sum=sum+(float(i[1])*float(i[2]))
        print(f"Der Gesamtpreis beträgt bei der aktuellen Einkaufslistte beträgt {sum}CHF")
                
                
        
        
        
        
        
            
    
    
    