



from turtle import *
shape("turtle")


Seite = numinput("Eingabefenster", "Bitte die Seitenlänge des Schweizerkreuzes angeben:")


penup()
forward(Seite/2)
left(90)
pendown()

pensize(0)
pencolor("red")
fillcolor("red")
begin_fill()



forward(Seite/2)
left(90)
forward(Seite)
left(90)
forward(Seite)
left(90)
forward(Seite)
left(90)
forward(Seite/2)
end_fill()

Seite2=Seite/4

penup()
left(90)
forward(Seite2/2)
right(90)
pendown()
fillcolor("white")
begin_fill()

forward(Seite2/2)
left(90)
forward(Seite2)
right(90)
forward(Seite2)
left(90)
forward(Seite2)
left(90)
forward(Seite2)
right(90)
forward(Seite2)
left(90)
forward(Seite2)
left(90)
forward(Seite2)
right(90)
forward(Seite2)
left(90)
forward(Seite2)
left(90)
forward(Seite2)
right(90)
forward(Seite2)
left(90)
forward(Seite2/2)
end_fill()
penup()
goto(0,0)
forward(Seite/20)
left(90)
speed(1)
while True:
    circle(Seite/20)
    


exitonclick()


