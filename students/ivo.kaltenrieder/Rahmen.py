from turtle import *

def rahmen(li, hi, fcr, fci):
    pendown()
    pencolor()
    fillcolor(fcr)
    begin_fill()
    
    
    penup()
    rt(90)
    fd(li*0.125)
    lt(90)
    back(li*0.125)
    pendown()
    
    
    la=li*1.25
    ha=hi*2
    for j in range(2):
        fd(la)
        lt(90)
        fd(ha)
        lt(90)
        
    
    
    penup()
    fd(li*0.125)
    lt(90)
    fd(li*0.125)
    rt(90)
    pendown() 
    end_fill()
    
    fillcolor(fci)
    begin_fill()
    for i in range(2):
        fd(li)
        lt(90)
        fd(hi)
        lt(90)
    end_fill()
    
rahmen(100, 25, "red", "white")
    
        
    