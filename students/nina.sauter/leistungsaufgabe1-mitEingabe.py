#Datei: leistungsaufgabe1.py
#Übung: leistungsaufgabe 1
#Beschreibung: py script für leistungsaufgabe 1 - Zeichnen Schweizerkreuz mit Eingabe                                                             
#Autor : Nina Sauter
#Datum: 26/02/2022
#Letzte Änderung: 27/02/2022
#python version: 3.7.9
#------------------------------------------------

from turtle import * 

hideturtle()
pensize(5)

def hintergrund(seitenlaenge):
    pencolor("red")
    fillcolor("red")
    begin_fill()
    #For loop mit range()-Funktion um gewisse Anzahl (4 mal) durch den angegebenen Code zu iterieren
    for i in range(4):
        forward(seitenlaenge)
        left(90)
    end_fill()
    
    #Stift heben, positionieren für weisses Kreuz + Ausrichtung Turtle nach rechts
    penup()
    forward(seitenlaenge / 2.46)
    left(90)
    forward(seitenlaenge / 5.3333)
    right(90)
    pendown()

def kreuz(seitenlaenge):
    #Länge und Tiefe der Arme des Schweizerkreuzes + Setzung der Farbe
    laenge = seitenlaenge / 5.3333
    tiefe = seitenlaenge / 4.571
    pencolor("white")
    fillcolor("white")
    begin_fill()
    #For loop mit range()-Funktion um gewisse Anzahl (4 mal) durch den angegebenen Code zu iterieren
    for i in range(4):
      forward(laenge)
      left(90)
      forward(tiefe)
      right(90)
      forward(tiefe)
      left(90)
    end_fill()


seitenlaenge = numinput("Seitenlänge Schweizerflagge", "Seitenlänge der Schweizerflagge: ")
hintergrund(seitenlaenge)
kreuz(seitenlaenge)
exitonclick()
