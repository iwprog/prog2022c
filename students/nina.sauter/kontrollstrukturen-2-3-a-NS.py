#Datei: kontrollstrukturen-2-3-a-NS.py
#Übung: Kontrollstrukturen 2.3 a 
#Beschreibung: py script für Übungsblatt Kontrollstrukturen 2.3 a - Funktion mit Rückgabewert und Parameter - Name                                                                        
#Autor : Nina Sauter
#Datum: 26/02/2022
#Letzte Änderung: 26/02/2022
#python version: 3.7.9
#------------------------------------------------


def guten_morgen(name):
    print("Guten Morgen", name + "!")
    


guten_morgen(name = input("Ihr Name? "))
