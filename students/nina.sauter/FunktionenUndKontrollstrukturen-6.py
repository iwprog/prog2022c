#übungsbeispiele zu funktionen und kontrollstrukturen - Aufgabe 6
from turtle import *


def zeichne_rahmen(laenge, breite, rahmen, fuellung):
    laenge = int(laenge)
    breite = int(breite)
    pensize(5)
    pencolor(rahmen)
    for i in range(2):
        forward(laenge)
        left(90)
        forward(breite)
        left(90)
    penup()
    forward(laenge / 4)
    left(90)
    forward(breite / 4)
    right(90)
    pendown()
    for i in range(2):
        forward(laenge / 2)
        left(90)
        forward(breite / 2)
        left(90)
    






laenge = input("Länge: ")
breite = input("Breite: ")
rahmen = input("Rahmenfarbe: ")
fuellung = input("Füllfarbe: ")
zeichne_rahmen(laenge, breite, rahmen, fuellung)