#Datei: adhoc-1-1-a-NS.py
#Übung: ad hoc 1.1 a
#Beschreibung: py script für ad hoc ex. 1.1 a - Zusammenstellung eines Datensatzes                                                                           
#Autor : Nina Sauter
#Datum: 16/02/2022
#Letzte Änderung: 16/02/2022
#python version: 3.7.9
#------------------------------------------------

print("Sauter", "Nina,", "Mühletalstrasse", "17,", "3110", "Münsingen")