#übungsbeispiele zu funktionen und kontrollstrukturen - Aufgabe 3

def get_billet_price(age, distance):
    base = 2
    perKm = 0.25 * int(distance)
    price = base + perKm
    if int(age) < 6:
        price = 0
        print(price)
    elif int(age) <= 16:
        print(price / 2)
    else:
        print(price)

age = input("Alter: ")
distance = input("Distanz in km: ")
get_billet_price(age, distance)