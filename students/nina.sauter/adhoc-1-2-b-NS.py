#Datei: adhoc-1-2-a-NS.py
#Übung: adhoc 1.2 - b
#Beschreibung: py script für adhoc ex. 1.2 b - Zeichnen eines Rechtecks                                                                           
#Autor : Nina Sauter
#Datum: 16/02/2022
#Letzte Änderung: 16/02/2022
#python version: 3.7.9
#------------------------------------------------

from turtle import *


forward(100)
left(90)
forward(50)
left(90)
forward(100)
left(90)
forward(50)
exitonclick()