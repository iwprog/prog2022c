#Datei: adhoc-1-3-b-NS.py
#Übung: ad hoc 1.3 b
#Beschreibung: py script für ad hoc ex. 1.3 b - Zeichnen 5 gefüllter Quadrate                                                                           
#Autor : Nina Sauter
#Datum: 16/02/2022
#Letzte Änderung: 16/02/2022
#python version: 3.7.9
#------------------------------------------------

from turtle import *
pensize(5)
pencolor("red")

#Quadrat 1 - Cyan
fillcolor("cyan")
begin_fill()
left(45)
forward(70)
right(90)
forward(70)
right(90)
forward(70)
right(90)
forward(70)
end_fill()

#Quadrat 2 - Gelb
fillcolor("yellow")
begin_fill()
right(160)
forward(70)
right(90)
forward(70)
right(90)
forward(70)
right(90)
forward(70)
end_fill()

#Quadrat 3 - Pink
fillcolor("pink")
begin_fill()
right(160)
forward(70)
right(90)
forward(70)
right(90)
forward(70)
right(90)
forward(70)
end_fill()

#Quadrat 4 - Blau
fillcolor("blue")
begin_fill()
right(160)
forward(70)
right(90)
forward(70)
right(90)
forward(70)
right(90)
forward(70)
end_fill()

#Quadrat 5 - Grün
fillcolor("green")
begin_fill()
right(170)
forward(70)
right(90)
forward(70)
right(90)
forward(70)
right(90)
forward(70)
end_fill()

exitonclick()
