from turtle import *

anzahl = int(input("Wählen Sie die Anzahl Dreiecke: "))
reihen = int(input("Wählen Sie die Anzahl Reihen: "))

for r in range(reihen):
    for c in range(anzahl):
        for s in range(3):
            forward(50)
            left(120)
        penup()
        forward(100)
        pendown()
    penup()
    home()
    right(90)
    forward(100)
    left(90)
    pendown()

exitonclick()
