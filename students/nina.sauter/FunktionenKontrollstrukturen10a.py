def herzfrequenz():
    age = input("Alter: ")
    maxfrequenz = 220 - int(age)
    print("Maximale Herzfrequenz: ", maxfrequenz)
    


def get_billet_price():
    print("""
    \tPreis für Fahrkarte berechnen
    \t-----------------------------
    """)
    age = int(input("\tAlter: "))
    if age < 6: 
        print("\tDie Fahrkarte kostet:", 0)
    else:
        pass
        distance = input("\tDistanz in km: ")
        basis = 2
        perKm = 0.25 * int(distance)
        price = basis + perKm
        if age < 16:
            print("\tDie Fahrkarte kostet:", price / 2)
        else:
            print("\tDie Fahrkarte kostet:", price)

option = None
while option != 0:
    print ("""
    ==================================================
    Programmübersicht:
    1 ... Preis für eine Fahrkarte berechnen
    2 ... Herzfrequenz berechnen

    0 ... Programm beenden
    ==================================================
        """)
    option = input("\tGewählte Option: ")
    option = int(option)
    if option == 1:
        get_billet_price()
    elif option == 2:
        herzfrequenz()
    elif option == 0:
        break
    else:
        print("\nUngültige Option!")