#Datei: kontrollstrukturen-2-3-c-NS.py
#Übung: Kontrollstrukturen 2.3 c 
#Beschreibung: py script für Übungsblatt Kontrollstrukturen 2.3 c - Funktion mit Rückgabewert und Parameter - inch to cm                                                                        
#Autor : Nina Sauter
#Datum: 26/02/2022
#Letzte Änderung: 26/02/2022
#python version: 3.7.9
#------------------------------------------------

def inch_to_cm(inch):
    inch = int(inch)
    cm = inch * 2.54
    print(inch, "inch entspricht", cm, "cm.")
    
inch_to_cm(inch = input("Eingabe in inch: "))