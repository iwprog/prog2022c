#Datei: adhoc-1-2-e-NS.py
#Übung: ad hoc 1.2 e
#Beschreibung: py script für ad hoc ex. 1.2 e - Zeichnen 3 farbiger Dreiecke mit Turtle                                                                           
#Autor : Nina Sauter
#Datum: 16/02/2022
#Letzte Änderung: 16/02/2022
#python version: 3.7.9
#------------------------------------------------

from turtle import *

#Rotes Dreieck
pencolor("red")
pensize(5)
forward(70)
left(120)
forward(70)
left(120)
forward(70)

#Grünes Dreieck
pencolor("green")
forward(70)
left(120)
forward(70)
left(120)
forward(70)

#Blaues Dreieck
pencolor("blue")
forward(70)
left(120)
forward(70)
left(120)
forward(70)

exitonclick()