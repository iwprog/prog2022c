#Datei: kontrollstrukturen-2-1-NS.py
#Übung: Kontrollstrukturen 2.1 
#Beschreibung: py script für Übungsblatt Kontrollstrukturen 2.1 - Funktion ohne Rückgabewert und Parameter                                                                           
#Autor : Nina Sauter
#Datum: 26/02/2022
#Letzte Änderung: 26/02/2022
#python version: 3.7.9
#------------------------------------------------


def guten_morgen():
    print("Guten Morgen!")
    

guten_morgen()