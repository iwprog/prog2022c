rows = 8
for col in range(rows):
    for x in range(col): #Aufsteigendes Dreieck bestehend aus Leerzeichen
        print(" ", end=" ") #Wird aktuelle Anzahl 'col' durchgearbeitet bevor Programm weitergeht zu nächster for-loop
    for j in range(col, rows): #Absteigendes Dreieck bestehend aus # mit range Spalte - Reihe
        print("#", end=" ")
    print()