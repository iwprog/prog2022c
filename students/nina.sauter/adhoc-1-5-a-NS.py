#Datei: adhoc-1-5-a-NS.py
#Übung: ad hoc 1.5 a
#Beschreibung: py script für ad hoc ex. 1.5 a - Dreiecke zeichnen mit Grösseninput                                                                
#Autor : Nina Sauter
#Datum: 16/02/2022
#Letzte Änderung: 19/02/2022
#python version: 3.7.9
#------------------------------------------------

from turtle import *
 
groesse = numinput("Seitenlänge Dreieck", "Bitte Seitenlänge angeben:")

pensize(5)
pencolor("red")
left(45)

#Dreieck 1 - kleinstes - Grün
fillcolor("green")
begin_fill()
forward(groesse)
left(120)
forward(groesse)
left(120)
forward(groesse)
end_fill()

#Dreieck 2 - mittel - blau
groesse = groesse * 2
fillcolor("blue")
begin_fill()
forward(groesse)
left(120)
forward(groesse)
left(120)
forward(groesse)
end_fill()

#Dreieck 3 - gross - gelb
groesse = groesse * 2
fillcolor("yellow")
begin_fill()
forward(groesse)
left(120)
forward(groesse)
left(120)
forward(groesse)
end_fill()


exitonclick()