#Datei: leistungsaufgabe1.py
#Übung: leistungsaufgabe 1
#Beschreibung: py script für leistungsaufgabe 1 - Zeichnen Schweizerkreuz                                                                
#Autor : Nina Sauter
#Datum: 26/02/2022
#Letzte Änderung: 27/02/2022
#python version: 3.7.9
#------------------------------------------------

from turtle import * 

hideturtle()
pensize(5)

def hintergrund():
    pencolor("red")
    fillcolor("red")
    begin_fill()
    #For loop mit range()-Funktion um gewisse Anzahl (4 mal) durch den angegebenen Code zu iterieren
    for i in range(4):
        forward(400)
        left(90)
    end_fill()
    
    #Stift heben, positionieren für weisses Kreuz + Ausrichtung Turtle nach rechts
    penup()
    forward(160)
    left(90)
    forward(87)
    right(90)
    pendown()


def kreuz():
    pencolor("white")
    fillcolor("white")
    begin_fill()
    #For loop mit range()-Funktion um gewisse Anzahl (4 mal) durch den angegebenen Code zu iterieren
    for i in range(4):
      forward(75)
      left(90)
      forward(75)
      right(90)
      forward(75)
      left(90)
    end_fill()


hintergrund()
kreuz()
exitonclick()