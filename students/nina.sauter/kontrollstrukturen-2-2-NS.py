#Datei: kontrollstrukturen-2-2-NS.py
#Übung: Kontrollstrukturen 2.2 
#Beschreibung: py script für Übungsblatt Kontrollstrukturen 2.2 - Funktion ohne Rückgabewert mit Parameter                                                                           
#Autor : Nina Sauter
#Datum: 26/02/2022
#Letzte Änderung: 26/02/2022
#python version: 3.7.9
#------------------------------------------------


def guten_morgen(name):
    print("Guten Morgen", name + "!")
    

guten_morgen("Ana")
