#Datei: adhoc-2-2-b-NS.py
#Übung: ad hoc 2.2 b
#Beschreibung: py script für ad hoc ex. 2.2 b - Dreiecke zeichnen mit Grösseninput inkl. Funktionen                                                                
#Autor : Nina Sauter
#Datum: 22/02/2022
#Letzte Änderung: 22/02/2022
#python version: 3.7.9
#------------------------------------------------

from turtle import *

pensize(5)
pencolor("red")
left(45)

def dreieck():
    groesse = numinput("Seitenlänge Dreieck", "Bitte Seitenlänge angeben:")
    begin_fill()
    forward(groesse)
    left(120)
    forward(groesse)
    left(120)
    forward(groesse)
    end_fill()

#Dreieck 1 - Grün
fillcolor("green")
dreieck()

#Dreieck 2 - blau
fillcolor("blue")
dreieck()

#Dreieck 3 - gelb
fillcolor("yellow")
dreieck()
exitonclick()