#Datei: kontrollstrukturen-2-3-b-NS.py
#Übung: Kontrollstrukturen 2.3 b 
#Beschreibung: py script für Übungsblatt Kontrollstrukturen 2.3 b - Funktion mit Rückgabewert und Parameter - Rechteck                                                                        
#Autor : Nina Sauter
#Datum: 26/02/2022
#Letzte Änderung: 26/02/2022
#python version: 3.7.9
#------------------------------------------------

def flaeche_rechteck(laenge, breite):
    flaeche = laenge * breite
    print("Die Fläche beträgt", flaeche, "m2.")
    
flaeche_rechteck(10, 20)