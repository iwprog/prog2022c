#Datei: adhoc-1-3-a-NS.py
#Übung: ad hoc 1.3 a
#Beschreibung: py script für ad hoc ex. 1.3 a - Zeichnen drei gefüllte Dreiecke                                                                           
#Autor : Nina Sauter
#Datum: 16/02/2022
#Letzte Änderung: 16/02/2022
#python version: 3.7.9
#------------------------------------------------


from turtle import *

pencolor("red")
pensize(5)

#Dreieck 1 - cyan
fillcolor("cyan")
begin_fill()
forward(100)
left(120)
forward(100)
left(120)
forward(200)
left(120)
end_fill()

#Dreieck 2 - gelb
fillcolor("yellow")
begin_fill()
forward(100)
left(120)
forward(100)
end_fill()
forward(100)

#Dreieck 3 - grün
fillcolor("green")
begin_fill()
left(120)
forward(100)
left(120)
forward(100)
end_fill()
exitonclick()