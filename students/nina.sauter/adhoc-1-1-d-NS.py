#Datei: adhoc-1-1-d-NS.py
#Übung: ad hoc 1.1 d
#Beschreibung: py script für ad hoc ex. 1.1 d - Umkehren eines Strings                                                                           
#Autor : Nina Sauter
#Datum: 16/02/2022
#Letzte Änderung: 16/02/2022
#python version: 3.7.9
#------------------------------------------------



print("Hello"[::-1])
#Erklärung:
#[::-1] -> Beginne an Ende der Zeichenkette (bei o) und Ende am Anfang (bei H).
# -1 -> bedeutet einen Schritt rückwärts bewegen

