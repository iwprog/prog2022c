#Datei: adhoc-1-2-a-NS.py
#Übung: ad hoc 1.2 a
#Beschreibung: py script für ad hoc ex. 1.2 a - Zeichnen eines Quadrats mit Turtle                                                                           
#Autor : Nina Sauter
#Datum: 16/02/2022
#Letzte Änderung: 16/02/2022
#python version: 3.7.9
#------------------------------------------------

from turtle import *

forward(50)
left(90)
forward(50)
left(90)
forward(50)
left(90)
forward(50)
exitonclick()