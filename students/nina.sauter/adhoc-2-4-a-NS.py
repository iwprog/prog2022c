#Datei: adhoc-2-4-a-NS.py
#Übung: ad hoc 2.4 a
#Beschreibung: py script für ad hoc ex. 2.4 a - Volumen Zylinder                                                                
#Autor : Nina Sauter
#Datum: 26/02/2022
#Letzte Änderung: 26/02/2022
#python version: 3.7.9
#------------------------------------------------

from math import pi, pow

def vol_zylinder(radius, hoehe):
    kreisflaeche = pi * pow(int(radius), 2) #Berechnung Kreisfläche - input radius umwwandlung str -> int
    vol = kreisflaeche * int(hoehe) # input hoehe umwwandlung str -> int
    print("Das Volumen beträgt:", vol)
    
vol_zylinder(radius = input("Eingabe Radius: "), hoehe = input("Eingabe Höhe: "))