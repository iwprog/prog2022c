from urllib.request import urlopen
from inscriptis import get_text

with urlopen("https://www.gutenberg.org/cache/epub/6079/pg6079.txt")as source:
    content=get_text(source.read().decode("utf8"))

with open("gutenberg.txt", "w", encoding="utf8") as inhalt:
    inhalt.write(content)

with open("gutenberg.txt",encoding="utf8") as text:
    inhalt=text.read()
    print(inhalt)

    himmel=0
    freiheit=0
    spiel=0
    tanz=0
    for wort in inhalt.split():
        if "himmel" in wort.lower():
            himmel=himmel+1
        if "freiheit" in wort.lower():
            freiheit=freiheit+1
        if "spiel" in wort.lower():
            spiel=spiel+1
        if "tanz" in wort.lower():
            tanz=tanz+1
    
    print("Himmel: "+str(himmel)+"\nFreiheit: "+str(freiheit)+"\nSpiel: "+str(spiel)+"\nTanz: "+str(tanz))