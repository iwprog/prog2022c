 
summe=0
preis=0
nummer=1
while preis!="x":
    preis=input("Geben Sie den "+str(nummer)+". Preis: ")
    if preis.isdigit():
        preis=int(preis)
        nummer=nummer+1
        summe=summe+preis
if summe<100:
    print("Kein Rabatt, Gesamtpreis =", round(summe,1),"CHF")
elif 100<=summe<1000:
    print("5% Rabatt, Gesamtpreis =", round(summe*0.95,1),"CHF")
else:
    print("10% Rabatt, Gesamtpreis =", round(summe*0.9,1),"CHF")
