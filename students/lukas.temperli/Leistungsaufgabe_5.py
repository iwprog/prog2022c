from urllib.request import urlopen
from csv import writer
from csv import reader


link=input("Welche Datei möchten Sie öffnen?")
if "https://" in link:
    with urlopen(link) as f:
        file_content = f.read().decode("utf8")
        wortliste=file_content.split()
else:
    with open(link,encoding="utf8") as f:
        csv_reader=reader(f, delimiter=";")
        wortliste=[]
        file_content=""
        for row in csv_reader:
            for i in row:
                wortliste.append(i)
                file_content=file_content+i

wortzähler={}
for i in wortliste:
    if i in wortzähler:
        wortzähler[i]=wortzähler[i]+1
    else:
        wortzähler[i]=1
buchstabenzähler={}
for i in file_content:
    if i.lower() in buchstabenzähler:
        buchstabenzähler[i.lower()]=buchstabenzähler[i.lower()]+1
    else:
        buchstabenzähler[i.lower()]=1

with open("wortliste.csv","w",encoding="utf8") as g:
    csv_writer=writer(g,delimiter=";")
    for wort in sorted(wortzähler):
        csv_writer.writerow([wort,wortzähler[wort]])    
with open("buchstabenliste.csv","w",encoding="utf8") as b:
    csv_writer=writer(b,delimiter=";")
    for buchstabe in sorted(buchstabenzähler):
        csv_writer.writerow([buchstabe,buchstabenzähler[buchstabe]]) 

    