from turtle import *
speed(10)


def buchstabe_p():
    
    fd(300);lt(90);fd(400);lt(90);fd(300);lt(90);fd(400);lt(90)

    forward(100)
    left(90)

    fillcolor("black")
    begin_fill()
    forward(350)
    right(90)
    forward(30)
    right(90)
    forward(350)
    right(90)
    forward(30)
    right(90)
    end_fill()

    forward(350)
    right(90)
    forward(30)

    begin_fill()
    forward(20)
    circle(-100,180)
    forward(20)
    right(90)
    forward(30)
    right(90)
    forward(20)
    circle(70,180)
    forward(20)
    right(90)
    forward(30)
    end_fill()

    back(350)
    right(90)
    forward(170)


buchstabe_p()
exitonclick()