l_input = "Der Tag begann sehr gut! Der Morgen war schön."

def word_stat(text):
    output={}
    text=text.lower()
    text=text.split()
    for index,i in enumerate(text):
        if "." in i or "!" in i:
            text[index]=i[:-1]
    for wort in text:
        if wort not in output:
            output[wort]=1
        else:
            output[wort]=output[wort]+1
    return output


print(word_stat(l_input))