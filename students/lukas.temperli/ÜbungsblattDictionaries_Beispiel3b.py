alphabet={"a":"Alfa","b":"Bravo","c":"Charlie","d":"Delta","e":"Echo","f":"Foxtrot","g":"Golf","h":"Hotel","i":"India","j":"Juliett","k":"Kilo","l":"Lima","m":"Mike","n":"November","o":"Oscar","p":"Papa","q":"Quebec","r":"Romeo","s":"Sierra","t":"Tango","u":"Uniform","v":"Victor","w":"Whiskey","x":"X-ray","y":"Yankee","z":"Zulu"}

wort=input("Welches Wort soll ich buchstabieren?")
wort=wort.lower()
while True:
    ausgabe=""
    for i in wort:
        if i not in alphabet:
            if i=="ö":
                ausgabe=ausgabe+alphabet["o"]+"-"+alphabet["e"]+"-"
            if i=="ä":
                ausgabe=ausgabe+alphabet["a"]+"-"+alphabet["e"]+"-"
            if i=="ü":
                ausgabe=ausgabe+alphabet["u"]+"-"+alphabet["e"]+"-"
        else:
            ausgabe=ausgabe+alphabet[i]+"-"
    print(ausgabe[:-1])
    break
