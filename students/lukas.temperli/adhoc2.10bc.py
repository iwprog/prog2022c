from turtle import *
pencolor("blue")
fillcolor("light blue")
speed(20)

penup()
back(600)


dreiecke=int(numinput("Dreiecke","Wie viele Dreiecke pro Reihe möchten Sie?:"))
for e in range(int(numinput("Dreiecke","Wie viele Reihen möchten Sie?:"))):
    for i in range(dreiecke):
        pendown()
        begin_fill()
        for y in range(3):
            forward(100)
            left(120)
        end_fill()
        penup()
        forward(150)
    back(150*dreiecke)
    right(90)
    forward(150)
    left(90)
    
exitonclick()