from json import dumps,loads

berge={}

try:
    with open("berge.json",encoding="utf8") as j:
        print("Dokument wird erweitert")
        json_string=j.read()
        berge=loads(json_string)
except FileNotFoundError:
    print("Datei wird neu erstellt.")
for i in range(3):
    berg=input("Name des "+str(i+1)+". Berges:")
    höhe=input("Höhe:")
    berge[berg]=höhe
bergliste=sorted(berge)
with open("berge.json","w",encoding="utf8") as f:
    json_string=dumps(berge)
    f.write(json_string)
for i in bergliste:
    print(i,"ist",berge[i],"m ("+str(int(round((int(berge[i])*3.28),0)))+" ft) hoch.")
