eingabetext="David Mayer von der HTW hat um 9:00 seine Wohnung in Chur verlassen."
blacklist=["david", "mayer", "htw", "chur"]
analyse=eingabetext.split()
ausgabe=""
for i in analyse:
    if i.lower() in blacklist:
        ausgabe=ausgabe+len(i)*"*"+" "
    else:
        ausgabe=ausgabe+i+" "
print(eingabetext)
print(ausgabe)