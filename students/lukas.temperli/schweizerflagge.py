from turtle import *
#Grundeinstellungen
pencolor("red")
fillcolor("red")
hideturtle()
speed(10)
#Die Länge wird als Variable l bezeichnet, um Rechnerei zu vermeiden
#und Vorarbeit für die zweite Aufgabe zu leisten.
l=400

#Die Turtle wird in Position gebracht, sodass die Flagge am Ende zentriert ist
penup()
right(90)
forward(int(l/2))
right(90)
pendown()

#Das Quadrat wird gezeichnet
begin_fill()
forward(int(l/2))
right(90)
forward(l)
right(90)
forward(l)
right(90)
forward(l)
right(90)
forward(int(l/2))
end_fill()

#Die Turtle wird in Position für das Kreuz gebracht
pencolor("white")
fillcolor("white")
begin_fill()
penup()
right(90)
forward(l/5)
pendown()


#Das Kreuz wird gezeichnet
right(90)
forward(l/10)
left(90)
forward(l/5)
right(90)
forward(l/5)
left(90)
forward(l/5)
left(90)
forward(l/5)
right(90)
forward(l/5)
left(90)
forward(l/5)
left(90)
forward(l/5)
right(90)
forward(l/5)
left(90)
forward(l/5)
left(90)
forward(l/5)
right(90)
forward(l/5)
left(90)
forward(l/10)
end_fill()


exitonclick()