age = 18
def legal_status(alter):
    if alter <0:
        return "ungeboren"
    elif alter <=6:
        return "geschäftsunfähig"
    elif alter <=14:
        return "unmündig"
    elif alter <18:
        return "mündig minderjährig"
    elif alter >=18:
        return "volljährig"


print("Mit", age, "ist man", legal_status(age) + ".")