def darstellung(eintrag):
    return "\n"+eintrag["Name"]+" ist "+str(eintrag["Höhe"])+" Meter hoch und gehört zum "+eintrag["Gebirge"]+" Gebirge."

bergliste=[]
for i in range(3):
    currentberg={}
    berg=input("Name des "+str(i+1)+". Berges:")
    höhe=input("Höhe:")
    gebirge=input("Gebirge:")
    currentberg["Name"]=berg
    currentberg["Höhe"]=höhe
    currentberg["Gebirge"]=gebirge
    bergliste.append(currentberg)


sortierliste=[]
sortieren=input("Sortieren nach (1) Berg, (2) Höhe oder (3) Gebirge:")
if sortieren=="1":
    aufab=input("Nach Berg (1) aufsteigend, oder (2) absteigend sortieren?:")
    for i in bergliste:
        sortierliste.append(i["Name"])
    sortierliste=sorted(sortierliste)
    if aufab=="2":
        sortierlisteab=[]
        for i in sortierliste:
            sortierlisteab.insert(0,i)
    else:
        sortierlisteab=sortierliste
    for name in sortierlisteab:
        for eintrag in bergliste:
            if eintrag["Name"]==name:
                print(darstellung(eintrag))
elif sortieren=="2":
    aufab=input("Nach Höhe (1) aufsteigend, oder (2) absteigend sortieren?:")
    for i in bergliste:
        sortierliste.append(int(i["Höhe"]))
    sortierliste=sorted(sortierliste)
    print(sortierliste)

    if aufab=="2":
        sortierlisteab=[]
        for i in sortierliste:
            sortierlisteab.insert(0,i)
    else:
        sortierlisteab=sortierliste
    for höhe in sortierlisteab:
        for eintrag in bergliste:
            if int(eintrag["Höhe"])==int(höhe):
                print(darstellung(eintrag))
elif sortieren=="3":
    aufab=input("Nach Gebirge (1) aufsteigend, oder (2) absteigend sortieren?:")
    for i in bergliste:
        sortierliste.append(i["Gebirge"])
    sortierliste=sorted(sortierliste)
    if aufab=="2":
        sortierlisteab=[]
        for i in sortierliste:
            sortierlisteab.insert(0,i)
    else:
        sortierlisteab=sortierliste
    for gebirge in sortierlisteab:
        for eintrag in bergliste:
            if eintrag["Gebirge"]==gebirge:
                print(darstellung(eintrag))



