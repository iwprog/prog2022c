from json import dumps,loads
from csv import writer

liste=[]
try:
    with open("einkaufsliste.json",encoding="utf8") as j:
        print("Existierende Liste wird erweitert")
        json_string=j.read()
        liste=loads(json_string)
except FileNotFoundError:
    print("Datei wird neu erstellt.")

while True:
    auswahl=input("Was möchten Sie gerne tun?\n(1) Artikel hinzufügen\n(2) Artikel löschen\n(3) Artikel suchen\n(4) Einkaufsliste leeren\n(5) Einkaufsliste im csv format exportieren\n(6) Gesamtbetrag Einkauf berechnen\n(0) Exit\nAuswahl:")
    if auswahl=="0":
        with open("einkaufsliste.json","w",encoding="utf8") as g:
            json_string=dumps(liste)
            g.write(json_string)
        break
    elif auswahl=="1":
        name=input("Name des Produkts:")
        preis=input("Preis:")
        menge=input("Menge:")
        liste.append([name,preis,menge])
    elif auswahl=="2":
        löschen=input("Welchen Artikel möchten sie löschen?")
        kopie=liste
        for i in kopie:
            if i[0].lower()==löschen.lower():
                liste.remove(i)
                print("Artikel gelöscht")
    elif auswahl=="3":
        suche=input("Geben Sie hier den Suchbegriff ein:")
        ergebnis=""
        for i in liste:
            if suche in i[0].lower():
                ergebnis=ergebnis+i[0]+", "
        print("Gefundene Objekte: "+ergebnis[:-2])
    elif auswahl=="4":
        liste=[]
        print("Liste gelöscht")
    elif auswahl=="5":
        with open("einkaufsliste.csv","w",encoding="utf8") as e:
            csv_writer=writer(e,delimiter=";")
            csv_writer.writerow(["Produkt","Menge","Preis"])
            for i in liste:
                csv_writer.writerow(i)
            print("Liste wurde exportiert")
    elif auswahl=="6":
        resultat=0
        for i in liste:
            resultat=resultat+(float(i[1])*float(i[2]))
        print(f"Der Gesamtpreis der aktuellen liste beträgt {resultat} CHF")
            
    