from turtle import *
def dreieck(laenge,farbe):
    fillcolor(farbe)
    begin_fill()
    forward(laenge)
    left(120)
    forward(laenge)
    left(120)
    forward(laenge)
    end_fill()

speed(10)
right(90)
pencolor("red")
pensize(5)

dreieck(numinput("Länge","Geben Sie die Länge des Dreiecks ein"),"cyan")
dreieck(numinput("Länge","Geben Sie die Länge des Dreiecks ein"),"yellow")
dreieck(numinput("Länge","Geben Sie die Länge des Dreiecks ein"),"lime")

exitonclick()