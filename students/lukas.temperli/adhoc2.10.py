from turtle import *
def formen():
    form=""
    form=textinput("Formen","Welche Form möchten Sie?")
    for symbol in form:
        if symbol=="d":
            for i in range(1,4):
                forward(100)
                left(120)
            forward(100)
        elif symbol=="#":
            for i in range(1,5):
                forward(100)
                left(90)
            forward(100)
        elif symbol=="o":
            penup()
            forward(50)
            pendown()
            circle(50)
            penup()
            forward(50)
            pendown()
        
penup()
back(500)
pendown()
formen()
exitonclick()