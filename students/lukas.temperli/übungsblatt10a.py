def get_billet_preis(alter, strecke):
    preis=2+strecke*0.25 
    if alter<6:
        return 0.00
    elif alter<16:
        return preis/2
    else:
        return preis
def berechne_herzfrequenz(alter):
    return "Ihre maximale Herzfrequenz ist:"+str((220-alter))

    
eingabe=""
while eingabe!="0":
    print(50*"="+"\nProgrammübersicht:\n  1 ... Preis für eine Fahrkarte berechnen\n  2 ... Maximale Herzfrequenz berechnen\n  3 ... Mahngebühr für die Bibliothek berechnen\n\n  0 ... Programm beenden\n"+50*"=")
    eingabe=input("Gewählte Option:")
    if eingabe=="1":
        print("\nPreis für Fahrkarte berechnen\n"+50*"-")
        alter=input("Bitte geben Sie ihr Alter ein:")
        strecke=input("Wie viele km möchten Sie fahren?")
        if alter.isdigit():
            alter=int(alter)
        if strecke.isdigit():
            strecke=int(strecke)
        print("Ihr billet kostet:",get_billet_preis(alter,strecke),"CHF") 
    elif eingabe=="2":
        print("\nHerzfrequenz berechnen\n"+50*"-")
        print(berechne_herzfrequenz(int(input("Geben Sie ihr alter an:"))))
    elif eingabe=="3":
        print("Diese Aufgabe hatten wir nicht gelöst. Keine Ahnung, 98.63 CHF, oder so.")
        input("Drücken Sie enter, um zum Menü zu gelangen.")
    elif eingabe=="0":
        break
    else:
        print("ungültige Option!")