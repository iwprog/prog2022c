def guten_morgen(name):
    return(f'Guten morgen {name}!')
    
gruss = guten_morgen("Anna")
print(gruss)

# "return function" return values to callers of our function