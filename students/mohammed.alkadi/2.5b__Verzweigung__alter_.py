age = 5

def legal_status(alter):
    if alter > 18:
        return("volljährig")
    elif alter <= 6:
        return("geschäftsunfähig")
    elif alter <= 14:
        return("unmündig")
    elif alter <=16:
        return("mündig minderjährig")
    elif alter >=0:
        return("ungeboren")
    
print("Mit", age, "ist man", legal_status(age) + ".")
        
    
    

