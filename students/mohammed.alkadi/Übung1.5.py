from turtle import *

# Übung 1.5a

penup()
right(90)
forward(150)
left(90)
pendown()
Länge = numinput("Eingabefenster" , "Bitte Seitenlänge angeben:")

begin_fill()
fillcolor("sky blue")
pencolor("red")
pensize(4)
right(30)
forward(Länge)
right(120)
forward(Länge)
right(120)
forward(Länge)
end_fill()

#Yellow dreieck

begin_fill()
fillcolor("yellow")
left(60)
forward(Länge*2)
left(120)
forward(Länge*2)
left(120)
forward(Länge*2)
end_fill()

#light green dreieck

begin_fill()
fillcolor("light green")
forward(Länge/2)
left(120)
forward(Länge/2)
left(120)
forward(Länge/2)
end_fill()
