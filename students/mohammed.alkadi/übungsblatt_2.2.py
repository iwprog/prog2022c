def guten_morgen(name):
    print(f'Guten morgen {name}!')
    
guten_morgen("Anna")


# when a function has a prameter, we are obligated to pass a value for that prameter
# the value is "Anna"  