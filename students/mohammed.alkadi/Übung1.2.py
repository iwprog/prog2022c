from turtle import *
# übung 1.2a
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)

# übung 1.2e
penup()
forward(200)
left(180)
pendown()

pensize(5)
pencolor("red")
forward(100)
right(120)
forward(100)
right(120)
forward(100)

pencolor("blue")
forward(100)

right(120)
forward(100)
right(120)
forward(100)

pencolor("green")
right(60)
forward(100)
left(120)
forward(100)
left(120)
forward(100)
