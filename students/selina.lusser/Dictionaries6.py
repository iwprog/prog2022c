# Erweitern Sie das vorhergehende Programm, sodass dieses zus ̈atzlich
# zu der H ̈ohe noch das Gebirge entgegennimmt, und dieses auch an-
# schliessend ausgibt. Ausserdem sollen die Berge – je nach Wunsch
# des Anwenders – auf- oder absteigend sortiert nach (i) dem Name des
# Berges, (ii) der H ̈ohe oder, (iii) dem Gebirge ausgegeben werden.
# 1. Berg: Mt. Everest
# 1. H ̈ohe: 8848
# 1. Gebirge: Himalaya
# 2. Berg: ...
# 2. H ̈ohe: ...
# 2. Gebirge: ...
# ....
# Sortieren nach (1) Berg, (2) H ̈ohe oder (3) Gebirge: 2
# Nach H ̈ohe (1) aufsteigend, oder (2) absteigend sortieren?: 1
# Mt. Everest ist 8848 m hoch und geh ̈ort zum Himalaya Gebirge.

berg = {}
for name in range(3):
    name = input("name des Berges: ")
    hoehe = input("hoehe des Berges: ")
    gebirge = input("name des Gebirges: ")
    berg[name] = [hoehe, gebirge] #wieso hier nicht name[berg]=hoehe?


print(berg)
sortierte_liste=[]
sortieren=input("Sortieren nach (i) Berg, (ii) Höhe oder (iii) Gebirge:")

if sortieren=="i":
    for n in sorted(berg):
        print(n)
elif sortieren =="ii":
    for n in sorted(berg.values(), key=lambda x:int(x[0]), reverse=True):
        print(n)
elif sortieren =="iii":
    for n in sorted(berg.values(), key=lambda x:x[1]):
        print(n)



