
eingabetext= "David Mayer von der FHGR hat um 9:00 seine Wohnung in Chur verlassen."
blacklist= ["david", "mayer", "fhgr", "chur"]
begriffe=eingabetext.split()
ausgabe=""
for i in begriffe:
    if i.lower() in blacklist:
        ausgabe=ausgabe+len(i)*"*"+" "
    else:
        ausgabe=ausgabe+i+" "
print(eingabetext)
print(ausgabe)
