# Entfernen Sie alle Lebensmittel aus dem Dictionary, welche die Zeichen-
# kette en im Namen enthalen
preisliste = {
"Brot" : 3.2,
"Milch": 2.1,
"Orangen": 3.75,
"Tomaten": 2.2,
}
preisliste2 = dict(preisliste)#weil sonst for nicht funktioniert

for element in preisliste2:
    if "en" in element:
        preisliste.pop(element)
print(preisliste)