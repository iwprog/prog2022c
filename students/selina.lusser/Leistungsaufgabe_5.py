# 1. Schreiben sie ein Programm, dass eine URL entgegenimmt (zum Beispiel
# https://www.ietf.org/rfc/rfc2616.txt) und
# (a) jedes Wort und die Anzahl der Vorkommnisse des Wortes berechnet.
# (b) berechnet wie oft jeder Buchstaben vorkommt.
# 2. Schreiben sie die Ergebnisse in zwei CSV Files:
# 3. Erweitern sie ihr Programm, sodass es die Ressourcenbezeichnung mittels
# input entgegennimmt und je nach Bezeichnung die Daten entweder aus dem
# Web holt (Bezeichnung startet mit ’https://’) oder aus einer Datei l̈adt.
from urllib.request import urlopen
from csv import writer
from csv import reader

link = input("Fügen Sie hier Ihren Link ein: ")

if "https://" in link:
    with urlopen(link) as f:
        file_content = f.read().decode("utf8")
        wortliste=file_content.split()
else:
    with open(link,encoding="utf8") as f:
        csv_reader=reader(f, delimiter=";")
        wortliste=[]
        file_content=""
        for row in csv_reader:
            for i in row:
                wortliste.append(i)
                file_content=file_content+i

wortzähler={}
for i in wortliste:
    if i in wortzähler:
        wortzähler[i]=wortzähler[i]+1
    else:
        wortzähler[i]=1
buchstabenzähler={}
for i in file_content:
    if i.lower() in buchstabenzähler:
        buchstabenzähler[i.lower()]=buchstabenzähler[i.lower()]+1
    else:
        buchstabenzähler[i.lower()]=1

with open("wort.csv","w",encoding="utf8") as a:
    csv_writer=writer(a,delimiter=";")
    for wort in sorted(wortzähler):
        csv_writer.writerow([wort,wortzähler[wort]])    
with open("buchstabe.csv","w",encoding="utf8") as b:
    csv_writer=writer(b,delimiter=";")
    for buchstabe in sorted(buchstabenzähler):
        csv_writer.writerow([buchstabe,buchstabenzähler[buchstabe]]) 

