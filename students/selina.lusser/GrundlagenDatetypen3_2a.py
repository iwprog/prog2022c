# Bitte verwenden Sie das Telefonbuch aus dem obigen Beispiel für die folgen-
# den Übungen:
# a. Schreiben Sie eine Funktion datensatz_ausgeben, welche den ersten Da-
# tensatz aus obigen Beispiel wie folgt ausgibt:
# Vorname : Ana
# Nachname: Skupch
# Phone : 123

daten1 = {"Vorname":"Ana", "Nachname": "Skupch","Phone":123}
daten2={"Vorname":"Tim","Nachname":"Kurz","Phone":732}
daten3={"Vorname":"Julia","Nachname":"Lang","Phone":912}

telefonbuch= []

telefonbuch.append(daten2)
telefonbuch.append(daten3)
telefonbuch.append(daten1)
def datensatz_ausgeben(tel):
    ausgabe=""
    for i,daten in enumerate(tel):
        ausgabe=ausgabe+"\n\nVorname: "+tel[i]["Vorname"]+"\nNachname: "+tel[i]["Nachname"]+"\nPhone: "+str(tel[i]["Phone"])
    return ausgabe
print(datensatz_ausgeben(telefonbuch))
