# e. Schreiben Sie ein Programm, welches Sie nach Lebensmittel und den zu-
# gehörigen Preisen fragt und diese so lange einem Dictionary hinzufügt,
# bis der Benutzer x eingibt. Im Anschluss soll das Dictionary ausgegeben
# werden.
# Lebensmittel: Brot
# Preis: 3.2
# Lebensmittel: Milch
# Preis: 2.1
# Lebensmittel: x
# Dictionary: {"Milch": "2.1", "Brot": "3.2"}
einkaufsliste={}

while True:
    produkt = input("Produkt für Einkaufsliste:")
    if produkt != "x":
        preis = input("Wie viel kostet es?")
        einkaufsliste[produkt]=preis
    else:
        print(einkaufsliste)
        break
    
