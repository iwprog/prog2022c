# - beispiel 5 (berge): erweitern sie das programm aus der letzten hausübung,
# sodass es sich die bereits eingegebenen berge merkt.
# - zu programmende sollen die berge in der datei "berge.json" gespeichert werden.
# - wenn "berge.json" existiert, sollen die berge aus der datei eingelesen werden.
from json import loads,dumps

berg = {}


for name in range(3):
    name = input("name des Berges: ")
    hoehe = input("hoehe des Berges: ")
    print("\n")
    berg[name] = hoehe 
    berg_liste = sorted(berg)
    
with open("berge.json", "w+", encoding = "utf8") as f:
    json_string = dumps(berg)
    f.write(json_string)
    
for n in berg_liste:
    print(n,"ist",berg[n],"m ("+str(int(round((int(berg[n])*3.28),0)))+" ft) hoch.")
    
with open("berge.json", encoding = "utf8") as f:
    json_string = f.read()
    berge = loads(json_string)