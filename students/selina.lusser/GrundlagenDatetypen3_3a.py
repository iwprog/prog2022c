# Entfernen Sie alle Datensätze aus dem Telefonbuch, bei denen eine 1 in
# der Telefonnummer vorkommt.
daten1={"Vorname":"Ana","Nachname":"Skupch","Phone":123}
daten2={"Vorname":"Tim","Nachname":"Kurz","Phone":732}
daten3={"Vorname":"Julia","Nachname":"Lang","Phone":912}
telefonbuch=[]
telefonbuch.append(daten1)
telefonbuch.append(daten2)
telefonbuch.append(daten3)

result_list = [x for x in telefonbuch if "1" not in str(x["Phone"])]
print(result_list)