
from turtle import *


def zeichne_rahmen(länge1, länge2, farbe1, farbe2):
    for i in range(2):
        if i == 0:
            fillcolor(farbe1)
            begin_fill()
            fd(länge1)
            left(90)
            fd(länge1)
            left(90)
            fd(länge1)
            left(90)
            fd(länge1)
            end_fill()

        else:
            fillcolor(farbe2)
            penup()
            left(90)
            fd((länge1-länge2)/2)
            left(90)
            fd((länge1-länge2)/2)
            right(90)
            pendown()
            begin_fill()
            fd(länge2)
            left(90)
            fd(länge2)
            left(90)
            fd(länge2)
            left(90)
            fd(länge2)
            end_fill()
            
            penup()
            fd(25)
            left(90)
            fd(länge1)
            pendown()
            
def anzahl():
     zahlen= int(numinput ("Wie viele Bilderrahmen wollen Sie Zeichnen?",""))
     for i in range(zahlen):
         zeichne_rahmen(100, 50, 'blue', 'cyan')
     

anzahl()
