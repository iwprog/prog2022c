# ad hoc Übung 2.2

from turtle import *

def dreieck(seitenlänge):
    begin_fill()
    fd(seitenlänge); right(120); fd(seitenlänge); right(120); fd(seitenlänge)
    end_fill()
    
pencolor("red")
pensize(5)
fillcolor("cyan")
right(30)
dreieck(100)

fillcolor("green")
dreieck(50)

fillcolor("yellow")
dreieck(200)
home()
