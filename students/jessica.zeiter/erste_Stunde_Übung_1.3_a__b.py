from turtle import *

# Übung 1.3 a: 3 Dreiecke farbig

pencolor("red")
pensize(4)

begin_fill()
fillcolor("blue")
right(30)
forward(100)

right(120)
forward(100)

right(120)
forward(100)
end_fill()

begin_fill()
fillcolor("yellow")
left(60)
forward(100)

left(120)
forward(100)

left(120)
forward(100)
end_fill()

begin_fill()
fillcolor("green")
forward(100)

left(120)
forward(100)

left(120)
forward(100)
end_fill()

# Übung 1.3 b: 5 Quadrate

penup()
backward(250)
left(90)
pendown()

pencolor("red")
pensize(5)
begin_fill()
fillcolor("blue")
right(50)
forward(80)

left(90)
forward(80)
left(90)
forward(80)
left(90)
forward(80)
end_fill()

begin_fill()
fillcolor("yellow")
left(100)
forward(80)

right(90)
forward(80)
right(90)
forward(80)
right(90)
forward(80)
end_fill()

begin_fill()
fillcolor("pink")
right(160)
forward(80)

right(90)
forward(80)
right(90)
forward(80)
right(90)
forward(80)
end_fill()

begin_fill()
fillcolor("dark blue")
right(160)
forward(80)

right(90)
forward(80)
right(90)
forward(80)
right(90)
forward(80)
end_fill()

begin_fill()
fillcolor("green")
right(160)
forward(80)

right(90)
forward(80)
right(90)
forward(80)
right(90)
forward(80)
right(90)
end_fill()

