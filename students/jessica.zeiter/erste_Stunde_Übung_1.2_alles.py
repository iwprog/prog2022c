from turtle import *
# Übung 1.2 a: Quadrat
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)

# Pfeil irgendwo anders weiterfahren; nicht sichtbar 
penup()
forward(300)
left(180)
pendown()

# Übung 1.2 b: Rechteck
forward(200)
left(90)
forward(100)
left(90)
forward(200)
left(90)
forward(100)

penup()
forward(100)
left(180)
pendown()

# Übung 1.2 c
pencolor("dark blue")
pensize(5)
left(225)
forward(100)

pencolor("red")
right(90)
forward(100)

pencolor("cyan")
left(90)
forward(100)

pencolor("black")
right(90)
forward(100)

# Übung 1.2 d: Dreieck

penup()
backward(700)
left(180)
pendown()

pensize(5)
pencolor("red")
right(135)
forward(100)

left(120)
forward(100)

left(120)
forward(100)

# Übung 1.2 e: 3 Dreiecke

penup()
forward(200)
left(180)
pendown()

pensize(5)
pencolor("red")
forward(100)

right(120)
forward(100)

right(120)
forward(100)

pencolor("dark blue")
forward(100)

right(120)
forward(100)

right(120)
forward(100)

pencolor("green")
right(60)
forward(100)

left(120)
forward(100)

left(120)
forward(100)

