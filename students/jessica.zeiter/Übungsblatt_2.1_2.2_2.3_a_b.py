# Übung 2.1
def guten_morgen():
    print("Guten Morgen!")

guten_morgen()

# Übung 2.2
def guten_morgen2(name):
    print("Guten Morgen", name+ "!")

guten_morgen2("Ana")

# Übung 2.3 a
def guten_morgen3 (name):
    return("Guten Morgen " + name + "!")

gruss= guten_morgen3("Ana")
print(gruss)

# Übung 2.3 b
def rechteck(länge, breite):
    return str(länge * breite)

fläche = rechteck(10, 15)
print("Die Fläche beträgt: " + fläche + "m2")
