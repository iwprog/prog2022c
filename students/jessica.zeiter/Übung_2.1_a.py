# ad hoc Übung 2.1 a

from turtle import *

def dreieck():
    fd(100); right(120); fd(100); right(120); fd(100)

right(30)
begin_fill()
pencolor("red")
pensize(3)
fillcolor("navy blue")
dreieck()
end_fill()

begin_fill()
fillcolor("green")
dreieck()
end_fill()

begin_fill()
fillcolor("yellow")
dreieck()
end_fill()
left(30)
