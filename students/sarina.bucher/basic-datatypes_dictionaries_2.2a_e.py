# Preisliste ausgeben

preisliste = {}
preisliste["Brot"] = 2.2
preisliste["Milch"] = 1.1
preisliste["Ketchup"] = 1.1
preisliste["Tee"] = 3.2
preisliste["Peanuts"] = 2.9

for lebensmittel in preisliste:
    print(lebensmittel, "kostet", preisliste[lebensmittel], "CHF")

# nur Lebensmittel, die billiger sind als 2.- ausgeben
print("\n")

for lebensmittel in preisliste:
    if preisliste[lebensmittel] < 2:
        print(lebensmittel, "kostet", preisliste[lebensmittel], "CHF")