# Dict Preisliste
# brot 3.2 // milch 2.1 // orangen 3.75 // tomaten 2.2

preisliste = {"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2}
print("a)", preisliste)

# preis milch ändern auf 2.05

preisliste["Milch"] = 2.05
print("b)", preisliste)

# eintrag brot entfernen

del preisliste["Brot"]
print("c)", preisliste)

# neuer Eintrag
# Tee 4.2 // Peanuts 3.9 // Ketchup 2.1

preisliste["Tee"] = 4.2
preisliste["Peanuts"] = 3.9
preisliste["Ketchup"] = 2.1
print("d)", preisliste)

# Schreiben Sie ein Programm, welches Sie nach Lebensmittel
# und den zugehörigen Preisen fragt und diese so lange
# einem Dictionary hinzufügt, bis der Benutzer x eingibt.
# Im Anschluss soll das Dictionary ausgegeben werden.

lebensmittel_preis_liste = {}
while True:
    lebensmittel = input("Lebensmittel: ")
    if lebensmittel == "x":
        break
    preis = input("Preis: ")
    if preis == "x":
        break
    else:
        lebensmittel_preis_liste[lebensmittel] = preis

print("e)", lebensmittel_preis_liste)