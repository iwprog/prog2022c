from turtle import * 

anzahl = numinput("Anzahl", "Wie viele Dreiecke? ")
anzahl = int(anzahl)

def dreieck():
    pensize(9)
    pencolor("brown")
    fillcolor("purple")
    begin_fill()
    fd(100)
    lt(120)
    fd(100)
    lt(120)
    fd(100)
    end_fill()
    
durchgang = 1
while durchgang <= anzahl:
    dreieck()
    lt(120)
    penup()
    fd(150)
    pendown()
    durchgang = durchgang + 1
    
exitonclick()