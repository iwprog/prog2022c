# Schreiben Sie eine Funktion, welche ein Alter entgegennimmt und je nach
# Alter den Wert volljährig oder minderjährig zurückgibt.
# age = 22
# print("Mit", age, "ist man", legal_status(age) + ".")
# Ausgabe: Mit 22 ist man volljährig.

# Erweitern Sie die Funktion, sodass diese zwischen geschäftsunfähig (bis
# inklusive 6 Jahre), unmündig (bis inklusive 14 Jahre), mündig minderjährig
# und volljährig unterscheidet. Weiters soll für ein negatives Alter ungeboren
# zurückgegeben werden.
# age = 5
# print("Mit", age, "ist man", legal_status(age) + ".")
# Ausgabe: Mit 5 ist man geschäftsunfähig.

age = input("Bitte Ihr Alter eingeben: ")
age= int(age)
    
    
if age >= 18:
    print("Mit", age, "ist man volljährig.")
elif age <= 6 and age >=0:
    print("Mit", age, "ist man geschäftsunfähig.")
elif age >= 14:
    print("Mit", age, "ist man mündig.")
elif age > 6:
    print("Mit", age, "ist man unmündig.")
else:
    print("Mit", age, "ist man ungeboren.")