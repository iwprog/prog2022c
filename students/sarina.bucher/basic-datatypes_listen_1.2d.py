# nur jene Jahreszeiten ausgeben, die mit den Buchstaben "er" enden

for jahreszeit in ["Frühling", "Sommer", "Herbst", "Winter"]:
    if jahreszeit[-2:] == "er":
        print(jahreszeit)