# Gegeben sei die folgende mehrdimensionale Liste:
# [["Zürich", 370000], ["Genf", 190000],
# ["Basel", 170000], ["Bern", 130000]]
# Schreiben Sie ein Programm, dass die Liste wie folgt
# aufbereitet ausgibt:
# Zürich hat 370000 Einwohner
# Genf hat 190000 Einwohner
# ...

for stadt, einwohnerzahl in [["Zürich", 370000], ["Genf", 190000],["Basel", 170000], ["Bern", 130000]]:
    print(stadt, "hat", einwohnerzahl, "Einwohner")