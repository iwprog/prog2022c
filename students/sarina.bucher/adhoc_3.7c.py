# nach value sortieren

stadtliste = {}
stadtliste["Zürich"] = 370000
stadtliste["Genf"] = 190000
stadtliste["Chur"] = 30000
stadtliste["Luzern"] = 81000


for stadt in stadtliste:
    print("UNSORTIERT:", stadt, "hat", stadtliste[stadt], "Einwohner")

print("\n")

neue_liste = []
for stadt in stadtliste:
    item = stadtliste[stadt], stadt
    neue_liste.append(item)
    neue_liste.sort()

for anzahl, stadt in neue_liste:
    print("SORTIERT:", stadt, "hat", anzahl, "Einwohner")