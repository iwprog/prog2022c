# Geben Sie alle Jahreszeiten und zusätzlich die letzten drei Buchstaben der
# Jahreszeit aus.

for jahreszeit in ["Frühling", "Sommer", "Herbst", "Winter"]:
    print(jahreszeit + " - " + jahreszeit[-3:])