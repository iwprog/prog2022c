from turtle import *

#Turtle soll nicht sichtbar sein
hideturtle()

#Eingabe
laenge = numinput ("Quadrat", "Seitenlänge des Quadrats:")

#Stiftfarbe
pencolor("red")

#Stift ausrichten
penup()
left(90)
forward(200)
pendown()

#Angaben Quadrat
fillcolor("red")
begin_fill()
left(90)
forward(200)
left(90)
forward(400)
left(90)
forward(400)
left(90)
forward(400)
left(90)
forward(200)
end_fill()

#Kreuz
pencolor("white")
fillcolor("white")

#Stift ausrichten
penup()
left(90)
forward(200)
right(90)
forward(125)
pendown()

#Kreuz Rechteck Eins
begin_fill()
right(90)
forward(50)
right(90)
forward(255)
right(90)
forward(100)
right(90)
forward(255)
right(90)
forward(100)
end_fill()

#Stift ausrichten
penup()
right(90)
forward(77.5)
pendown()

#Kreuz Rechteck Zwei
begin_fill()
left(90)
forward(77.5)
right(90)
forward(100)
right(90)
forward(255)
right(90)
forward(100)
right(90)
forward(255)
end_fill()