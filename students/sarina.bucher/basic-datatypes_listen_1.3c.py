# Schreiben Sie eine Funktion, welche aus einer Liste alle Texte entfernt,
# welche weniger als drei Buchstaben haben.

liste = ["Brot", "mit", "Zucker", "und", "Salz", "und", "Mehl", "und", "Kerzen"]

for wort in liste:
    if len(wort) > 3:
        print(wort)