# Liste mit allen Jahreszeiten 
jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]

print(jahreszeiten)

# Frühling aus Liste entfernen
jahreszeiten.remove("Frühling")

print(jahreszeiten)

# Langas der Liste hinzufügen
jahreszeiten.append("Langas")

print(jahreszeiten)