from turtle import *

laenge = numinput("Dreieck", "Bitte Seitenlänge angeben:")

pensize(5)
pencolor("red")
fillcolor("cyan")
begin_fill()
right(90)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()
fillcolor("yellow")
begin_fill()
laenge = laenge * 2
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()
fillcolor("lightgreen")
begin_fill()
laenge = laenge / 4
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()


