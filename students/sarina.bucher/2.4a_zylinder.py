# Schreiben Sie eine Funktion, die das Volumen eines Zylinders berechnet:
# Eingabe: Radius, Höhe
# Ausgabe: Volumen
# print('Das Volumen beträgt:', volumen(2,2))
# 25.132741228718345

# Hinweis: pi wird in python wie folgt verwendet
# from math import pi
# print(pi)

from math import pi

def volumen_zylinder(radius, hoehe):
    return(radius**2*hoehe*pi)

volumen = volumen_zylinder(2, 2)

print("Das Volumen beträgt: ", volumen)