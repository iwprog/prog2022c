#
print("Aufgabe 1.5a")
#
from turtle import *
shape("turtle")
pencolor("red")
pensize(5)
left(30)

l=numinput("Dreieck","Geben Sie die länge ein")

fillcolor("lime")
begin_fill()
forward(l)
left(120)
forward(l)
left(120)
forward(l)
end_fill()

l=2*l
fillcolor("cyan")
begin_fill()
forward(l)
left(120)
forward(l)
left(120)
forward(l)
end_fill()

l=2*l
fillcolor("yellow")
begin_fill()
forward(l)
left(120)
forward(l)
left(120)
forward(l)
end_fill()
exitonclick()