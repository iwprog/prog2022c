# Dictionnary mit CH Städten + Einwohnerzahlen
# Zürich hat 370000 Einwohner
# Genf hat 190000 Einwohner

cities={"Frauenfeld":30, "Winterthur":50, "Zürich":80, "Diessenhofen":10}

for i in sorted(cities):
    print(i, "hat", cities[i], "Einwohner")

sortedDict = sorted(cities.values())