print("a")
i = 1
while i <= 8:
    print("# " * i)
    i = i + 1

print("b")
i = 8
while i >= 1:
    print("# " * i)
    i = i - 1

print("c")
i = 8
while i >= 1:
    print(" " * (8 - i) + "# " * i)
    i = i - 1

print("j")
i = 11
while i >= 1:
    print(" " * (11-i) + "# " * i)
    i = i - 1