from turtle import *

def dreieck(seitenlaenge, stiftfarbe, fuellfarbe):
    pencolor(stiftfarbe)
    fillcolor(fuellfarbe)
    begin_fill()
    left(45)
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    left(185)
    end_fill()

pensize(3)
dreieck(50, "red", "light green")

dreieck(50*3, "red", "yellow")

dreieck(50*2, "red", "light blue")