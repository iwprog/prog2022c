from turtle import *
from math import pi

def zylinder_volumen(radius, höhe):
    return(radius**2*pi*höhe)

volumen=zylinder_volumen(2, 2)

print("Das Volumen beträgt:", volumen)