zusatz = 0.25

def get_billet_preis(alter, strecke):
    grundpreis = 2
    preis = grundpreis + zusatz * strecke
    
    if alter <= 16:
        preis = preis*0.5
    if alter < 6:
        preis = 0
    
    return preis

def berechne_eintrittspreis(typ, ermässigung, alter):
    preis = typ
    
    if typ == "Kurzzeitkarte":
        preis = 5
    if typ == "Nachmittagskarte":
        preis = 6
    if typ == "Tageskarte":
        preis = 10
    
    if ermässigung == "true":
        preis = preis*0.9
    else:
        preis = preis*1
    
    if alter <= 6:
        preis = 0
    if alter <= 12:
        preis = preis*0.5
    
    return(preis)

def berechne_mahngebuehr(anzahl, medien, versand):
    if anzahl == 1:
        preis = 2*medien
    if anzahl == 2:
        preis = 4*medien
    if anzahl >= 3:
        preis = 6*medien
    
    if versand == "true":
        preis = preis + 2
    else:
        print(preis)
    
    return(preis)

auswahl = 0
while auswahl != 9:
    print("==================================================")
    print("Programmübersicht")
    print("1 ... Preis für eine Fahrkarte berechnen")
    print("2 ... Eintritt für das Schwimmbad berechnen")
    print("3 ... Mahngebühr für die Bibliothek berechnen")
    print()
    print("0 ... Programm beenden")
    print("=================================================")
    
    auswahl = int(input("Auswahl: "))

if auswahl == 1:
    print("\n Preis für Fahrkarte berechnen")
    print()
    alter = int(input("Alter? "))
    strecke = float(input("Strecke? "))
    print("\n Die Fahrkarte kostet:", get_billet_preis(alter, strecke))

elif auswahl == 2:
    print("\n Eintritt für das Schwimmbad berechnen")
    print()
    typ = input("Kartentyp? ")
    ermässigung = input("Ermässigung true oder false? ")
    alter = int(input("Alter? "))
    print("\n Der Eintritt kostet:", berechne_eintrittspreis(typ, ermässigung, alter))

elif auswahl == 3:
    print("\n Mahngebühr für die Bibliothek berechnen")
    print()
    anzahl = int(input("Die wievielte Mahnung? "))
    medien = int(input("Wie viele ausgeliehene Medien? "))
    versand = input("Versand per Post = true? ")
    print("\n Die Mahngebühr beträgt:", berechne_mahngebuehr(anzahl, medien, versand))

elif auswahl != 0:
    print("Error")