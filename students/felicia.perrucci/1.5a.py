from turtle import *

laenge = numinput("Dreieck", "Länge des Dreiecks?")

pencolor("red")
pensize(3)

farbe = textinput("Dreieck", "Farbe?")

fillcolor(farbe)
begin_fill()
left(40)
forward(50)
left(120)
forward(50)
left(120)
forward(50)
end_fill()

farbe = textinput("Dreieck", "Farbe?")

fillcolor(farbe)
begin_fill()
right(120)
forward(200)
left(120)
forward(200)
left(120)
forward(200)
end_fill()

farbe = textinput ("Dreieck", "Farbe?")

fillcolor(farbe)
begin_fill()
right(120)
forward(100)
left(120)
forward(100)
left(120)
forward(100)
end_fill()
