#Variante 1
print("Lucas Järmann")
print("Plessurquai 21")
print("7000 Chur")
print("\n")

#Variante 2
Vorname = "Lucas"
Nachname = "Järmann"
Strasse = "Plessurquai"
Hausnummer = "21"
Postleitzahl = "7000"
Ort = "Chur"
print(Vorname,Nachname+"\n",Strasse,Hausnummer+"\n",Postleitzahl,Ort)
print("\n")

#Variante 3

print(Vorname, Nachname)
print(Strasse, Hausnummer)
print(Postleitzahl, Ort)
