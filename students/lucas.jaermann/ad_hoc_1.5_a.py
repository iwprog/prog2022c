from turtle import * 

Länge = numinput("Eingabefenster", "Bitte Seitenlänge angeben")

pensize(3)
pencolor("red")
speed(3)

#grünes Dreieck
fillcolor("green")
begin_fill()
lt(90)
fd(Länge)
rt(120)
fd(Länge)
rt(120)
fd(Länge)
end_fill()

#türkises Dreieck
Länge = Länge*2
fillcolor("cyan")
begin_fill()
lt(120)
fd(Länge)
rt(120)
fd(Länge)
rt(120)
fd(Länge)
end_fill()

#gelbes Dreieck
Länge = Länge*2
fillcolor("yellow")
begin_fill()
lt(120)
fd(Länge)
rt(120)
fd(Länge)
rt(120)
fd(Länge)
end_fill()
