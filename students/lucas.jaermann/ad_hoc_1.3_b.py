from turtle import *

#Stift
pencolor("red")
pensize(5)
speed(3)

#türkises Quadrat
fillcolor("cyan")
begin_fill()
rt(15)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
end_fill()

#gelbes Quadrat
fillcolor("yellow")
begin_fill()
lt(15)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
end_fill()

#pinkes Quadrat
fillcolor("magenta")
begin_fill()
lt(15)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
end_fill()

#blaues Quadrat
fillcolor("blue")
begin_fill()
lt(15)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
end_fill()

#grünes Quadrat
fillcolor("green")
begin_fill()
lt(15)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
lt(90)
fd(100)
end_fill()

exitonclick()