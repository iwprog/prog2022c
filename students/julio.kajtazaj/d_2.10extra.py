from turtle import *

form = textinput("Welche Formen sollen gezeichnet wertden?", "d=Dreieck, o=Kreis, q=Quadrat") 
speed(1000)

def dreieck():
    fillcolor("blue")
    begin_fill()
    forward(100)
    left(120)
    forward(100)
    left(120)
    forward(100)
    left(120)
    end_fill()
    forward(100)
    penup()
    forward(50)
    pendown()

def quadrat():
    fillcolor("blue")
    begin_fill()
    forward(100)
    left(90)
    forward(100)
    left(90)
    forward(100)
    left(90)
    forward(100)
    left(90)
    forward(100)
    end_fill()
    penup()
    forward(50)
    pendown()

def kreis():
    fillcolor("blue")
    begin_fill()
    circle(50)
    end_fill()
    penup()
    forward(100)
    pendown()


for i in form:
    if i == "q":
        quadrat()
    elif i == "o":
        kreis()
    elif i == "d":
        dreieck()
    else:
        print("Ungültige Form:", i)
    
        
