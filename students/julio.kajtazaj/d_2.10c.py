from turtle import *
anzahl= int(numinput("Anzahl Dreieck", "Anzahl Dreiecke pro Reihe?"))
reihe= int(numinput("Anzahl Reihen an Dreiecken", "Wieviele Reihen an Dreiecke?"))

speed(10)

def dreieck():
    fillcolor("blue")
    begin_fill()
    forward(100)
    left(120)
    forward(100)
    left(120)
    forward(100)
    left(120)
    end_fill()
    forward(100)
    penup()
    forward(50)
    pendown()

def quadrat():
    fillcolor("blue")
    begin_fill()
    forward(100)
    left(90)
    forward(100)
    left(90)
    forward(100)
    left(90)
    forward(100)
    left(90)
    forward(100)
    end_fill()
    penup()
    forward(50)
    pendown()

def kreis():
    fillcolor("blue")
    begin_fill()
    for i in range (360):
          forward(1)
          left(1)
          i=i+1
    end_fill()


def reihen():
    penup()
    right(180)
    forward(150*anzahl)
    left(90)
    forward(150)
    left(90)


for i in range (reihe):
    for i in range (anzahl):
        dreieck()

    reihen()

    