# Stundenwiederholung vom 1. März 2022

def funktionsname():
    print("Guten Morgen")

funktionsname()


def grüezi(name):
    print("Guten Morgen " + name)
    
def guten_morgen(name, zeit):
    if zeit <= 9:
        return "Guten Morgen " + name + "!"    
    elif zeit <= 18:
        return "Guten Tag " + name + "!"
    else:
        return "Guten Abend " + name + "!"
    

name = "Ana"
grüezi(name)
grüezi("Tom")
print(name)

print(guten_morgen("Ana", 9))
print(guten_morgen("Ana", 10))
print(guten_morgen("Ana", 22))

# Pause bis 9:05
