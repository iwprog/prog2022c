#Datei: Python-O.py
#Übung: Python O
#Beschreibung: py script für Python schreiben - O
#Autor : Nina Sauter
#Datum: 26/02/2022
#Letzte Änderung: 26/02/2022
#python version: 3.7.9
#------------------------------------------------

from turtle import *

def o():
    pensize(2)
    circle(80)
    penup()
    forward(100)
    pendown()
