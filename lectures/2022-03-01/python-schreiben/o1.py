from turtle import *
pensize(2)
speed(10)
pencolor("black")

def rahmen():
    fd(300)
    lt(90)
    fd(400)
    lt(90)
    fd(300)
    lt(90)
    fd(400)
    lt(90)

def o():
    rahmen()
    fillcolor("black")
    begin_fill()
    penup()
    fd(150)
    pendown()

    circle(150, 90)
    fd(100)
    circle(150, 180)
    fd(100)
    circle(150, 90)
    end_fill()

    penup()
    lt(90)
    fd(50)
    rt(90)
    pendown()

    fillcolor("white")
    begin_fill()
    circle(100, 90)
    fd(100)
    circle(100, 180)
    fd(100)
    circle(100, 90)
    end_fill()


    penup()
    rt(90)
    fd(50)
    lt(90)
    fd(150)
    pendown()
