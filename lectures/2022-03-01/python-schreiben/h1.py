from turtle import *
# Buchstabe H

def h():
    penup()
    right(90)
    forward(200)
    left(90)
    backward(150)
    pendown()

    pensize(2)
    forward(300)
    left(90)
    forward(400)
    left(90)
    forward(300)
    left(90)
    forward(400)

    penup()
    home()
    pendown()

    penup()
    right(90)
    forward(140)
    right(90)
    fd(60)
    right(90)
    pendown()

    pensize(11)
    pencolor("dark blue")
    fd(260)
    penup()
    backward(130)
    pendown()
    right(90)
    fd(120)
    left(90)
    fd(130)
    penup()
    backward(130)
    pendown()
    backward(130)
    right(90)
