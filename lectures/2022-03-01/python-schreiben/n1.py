from turtle import *

def n():
    pensize(10)
    penup()
    fd(80)
    left(90)
    pendown()
    fd(100)
    right(140)
    fd(130)
    left(140)
    fd(100)
    right(180)
    fd(100)
    penup()
    left(90)
    fd(100)
    pendown()
