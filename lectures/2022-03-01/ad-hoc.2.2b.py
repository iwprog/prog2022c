from turtle import *

def dreieck(laenge,farbe):
    fillcolor(farbe)
    begin_fill()
    fd(laenge)
    lt(120)
    fd(laenge)
    lt(120)
    fd(laenge)
    end_fill()

right(90)
pensize(10)
pencolor("red")

dreieck(numinput("Laenge","eingeben"),"cyan")
dreieck(numinput("Laenge","eingeben"),"yellow")
dreieck(numinput("Laenge","eingeben"),"lightgreen")

# because we define the function in line 3 with a prameter "(laenge,farbe):", we need call the function back, otherwise it will not be executed.
