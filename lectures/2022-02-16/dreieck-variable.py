from turtle import *

laenge = numinput("Dreieck", "Länge des Dreiecks?")

farbe = textinput("Dreieck", "Farbe?")
pencolor(farbe)

forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
left(120)

farbe = textinput("Dreieck", "Farbe?")
pencolor(farbe)

laenge = laenge * 2
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
left(120)

farbe = textinput("Dreieck", "Farbe?")
pencolor(farbe)

laenge = laenge * 2
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
left(120)
