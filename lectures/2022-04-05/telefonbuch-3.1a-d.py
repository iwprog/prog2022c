nutzer1={"Vorname":"Ana",
         "Nachname":"Skupch",
         "Phone":123}
"""Alternative Implementierung"""
nutzer1_alternativ = ["Ana", "Skupch", 123]

nutzer2={"Vorname":"Tim","Nachname":"Kurz","Phone":732}
nutzer3={"Vorname":"Julia","Nachname":"Lang","Phone":912}

telefonbuch=[]
telefonbuch.append(nutzer1)
telefonbuch.append(nutzer2)
telefonbuch.append(nutzer3)

"""
Auskommentiert

telefonbuch_d=[]
while True:
    nutzer={}
    vorname=input("Vorname:")
    if vorname=="x":
        break
    nutzer["Vorname"]=vorname
    nachname=input("Nachname:")
    nutzer["Nachname"]=nachname
    phone=input("Phone:")
    nutzer["Phone"]=phone
    telefonbuch_d.append(nutzer)
"""

def datensatz_ausgeben(tel):
    ausgabe=""
    for i,daten in enumerate(tel):
        ausgabe=(ausgabe+"\n\nVorname: "+tel[i]["Vorname"]+"\nNachname: "+
                 tel[i]["Nachname"]+"\nPhone: "+str(tel[i]["Phone"]))
    return ausgabe

def datensatz_ausgeben2(tel):
    ausgabe=""
    for eintrag in tel:
        ausgabe=(ausgabe+"\n\nVorname: "+eintrag["Vorname"]+"\nNachname: "+
                 eintrag["Nachname"]+"\nPhone: "+str(eintrag["Phone"]))
    return ausgabe

""" Zuerst filtern"""
liste = []
for eintrag in telefonbuch:
    # print(eintrag)
    if "1" not in str(eintrag["Phone"]):
        liste.append(eintrag)

print(liste)
