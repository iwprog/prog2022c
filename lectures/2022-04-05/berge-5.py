berg = {}
for durchlauf in range(3):
    name = input("name des Berges: ")
    hoehe = input("hoehe des Berges: ")
    berg[name] = hoehe   #wieso hier nicht name[berg]=hoehe?
    berg_liste = sorted(berg)
    
for n in berg_liste:
    print(n,"ist",berg[n],"m ("+str(int(round((int(berg[n])*3.28),0)))+" ft) hoch.")
    
""" alternative varianten """
for name, höhe in sorted(berg.items()):
    ft = round(int(höhe)*3.28)
    print(f"{name} ist {höhe} m ({ft}) hoch.")
