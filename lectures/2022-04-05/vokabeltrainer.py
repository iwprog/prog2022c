dict = {"Apfel":"apple", "Ananas":"pineapple", "Ingwer":"ginger", "Drachenfrucht":"dragonfruit", "Wassermelone":"watermelon",
        "Birne":"pear", "Traube":"grape", "Banane":"banana", "Zitrone":"lemon", "Passionsfrucht":"passion fruit"}

anzahl = 0
for begriff in dict:
    frage = input("Was heisst " + begriff + ": ")
    if frage == dict[begriff]:
        print("RICHTIG!")
        anzahl = anzahl + 1
    else:
        print("FALSCH!")

print("Sie haben", anzahl, "von 10 Vokabeln richtig beantwortet!")