from urllib.request import urlopen

stadt = input("Für welche Stadt wollen Sie die Temperatur wissen? ")

firstrun = True
if firstrun:
    with urlopen("https://meteo.search.ch/" + stadt.lower()) as f:
        content = f.read().decode("utf8")
        
    with open("chur.html", "w") as f:
        f.write(content)
else:
    with open("chur.html") as f:
        content = f.read()
        
vorher, nachher = content.split("""Lufttemperatur 2 m über Boden">""", 1)
temp, nachher = nachher.split(" °C") 
print(temp, "°C")

