from urllib.request import urlopen
from csv import reader, writer

with urlopen("https://www.gutenberg.org/cache/epub/6079/pg6079.txt") as f:
    file_content = f.read().decode("utf8")
print(file_content)

"""
Erstellen Sie eine CSV Datei mit den Häufigkeiten aller Wörter, in dem
obenstehenden Text.
"""
häufigkeit = {}
worte = file_content.split()
for wort in worte:
    if wort in häufigkeit:
        häufigkeit[wort] = häufigkeit[wort] + 1
    else:
        häufigkeit[wort] = 1
        
"""
schreibe die häufigkeiten in ein csv
"""
with open("häufigkeiten.csv", "w", encoding="utf8") as f:
    csv_writer = writer(f, delimiter=";")
    for wort in sorted(häufigkeit):
        csv_writer.writerow([wort, häufigkeit[wort]])
