"""
Aufgabenstellung:
-----------------

Schreiben Sie eine Applikation, in welcher der Benutzer Namen Telefon# zuordnen
kann.
- im Anschluss sollen diese ausgegeben werden.

NEU:
- mit Programmende sollen die bestehenden Telefon# in einer CSV Datei gespeichert
  werden.
- wenn man das Programm erneut startet, sollen diese ins Dictionary eingelesen
  werden, bevor der/die Benutzer(in) neue Nummern eingeben kann.

bis 11:15
"""
from urllib.request import urlopen
from csv import reader
from csv import writer
from inscriptis import get_text

nutzer1={"Vorname":"Ana","Nachname":"Skupch","Phone":123}
nutzer2={"Vorname":"Tim","Nachname":"Kurz","Phone":732}
nutzer3={"Vorname":"Julia","Nachname":"Lang","Phone":912}

"""Telefonbuch einlesen"""
telefonbuch=[]
with open("telefonbuch.csv", "r", encoding="utf8") as f:
     csv_reader  = reader(f, delimiter=";")
     for vorname, nachname, phone in csv_reader:
         nutzer = {"Vorname":vorname,"Nachname": nachname, "Phone": phone}
         telefonbuch.append(nutzer)
 
# exit(0)
#telefonbuch.append(nutzer1)
#telefonbuch.append(nutzer2)
#telefonbuch.append(nutzer3)


def datensatz_ausgeben(buch):
    ausgabe=""
    for i,nutzer in enumerate(buch):
        ausgabe=ausgabe+"\n\nVorname: "+buch[i]["Vorname"]+"\nNachname: "+buch[i]["Nachname"]+"\nPhone: "+str(buch[i]["Phone"])
    return ausgabe
print(datensatz_ausgeben(telefonbuch))

""" Speichern """
with open("telefonbuch.csv", "w", encoding="utf8") as f:
    csv_writer = writer(f, delimiter=";")
    for eintrag in telefonbuch:
        print(eintrag)
        csv_writer.writerow([eintrag["Vorname"], eintrag["Nachname"], eintrag["Phone"]])
