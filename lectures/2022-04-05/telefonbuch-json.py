"""
Aufgabenstellung:
-----------------

Schreiben Sie eine Applikation, in welcher der Benutzer Namen Telefon# zuordnen
kann.
- im Anschluss sollen diese ausgegeben werden.

NEU:
- mit Programmende sollen die bestehenden Telefon# in einer CSV Datei gespeichert
  werden.
- wenn man das Programm erneut startet, sollen diese ins Dictionary eingelesen
  werden, bevor der/die Benutzer(in) neue Nummern eingeben kann.

bis 11:15
"""
from urllib.request import urlopen
from json import loads, dumps
from inscriptis import get_text

# nutzer1={"Vorname":"Ana","Nachname":"Skupch","Phone":123}
# nutzer2={"Vorname":"Tim","Nachname":"Kurz","Phone":732}
# nutzer3={"Vorname":"Julia","Nachname":"Lang","Phone":912}

"""Telefonbuch einlesen"""
with open("saved.json", encoding="utf8") as f:
    text = f.read()
    telefonbuch = loads(text)

# exit(0)
#telefonbuch = []
#telefonbuch.append(nutzer1)
#telefonbuch.append(nutzer2)
#telefonbuch.append(nutzer3)


def datensatz_ausgeben(buch):
    ausgabe=""
    for i,nutzer in enumerate(buch):
        ausgabe=ausgabe+"\n\nVorname: "+buch[i]["Vorname"]+"\nNachname: "+buch[i]["Nachname"]+"\nPhone: "+str(buch[i]["Phone"])
    return ausgabe
print(datensatz_ausgeben(telefonbuch))

""" Speichern """
with open("saved.json","w",encoding="utf8") as j:
    json_string=dumps(telefonbuch)
    j.write(json_string)
