from urllib.request import urlopen


with urlopen("https://www.gutenberg.org/cache/epub/6079/pg6079.txt") as f:
    file_content = f.read().decode("utf8")
print(file_content)

häufigkeit = {}
worte = file_content.split()
for wort in worte:
    if wort in häufigkeit:
        häufigkeit[wort] = häufigkeit[wort] + 1
    else:
        häufigkeit[wort] = 1

if not "Tanz" in häufigkeit:
    häufigkeit["Tanz"] = 0
    
print(str(häufigkeit["Himmel"]) + "x Himmel \n" + str(häufigkeit["Freiheit"]) + "x Freiheit \n" + str(häufigkeit["Spiel"]) + "x Spiel \n" + str(häufigkeit["Tanz"]) + "x Tanz \n")

