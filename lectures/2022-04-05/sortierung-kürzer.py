
berge = [ ["Berg2", 3200, "G1"],
          ["Berg1", 1200, "G1"],
          ["Berg3", 4200, "G2"]]

"""Standardsortierung erfolgt nach dem ersten Element """
for berg, höhe, gebirge in sorted(berge):
    print(f"{berg} ist {höhe} m hoch und im Gebirge {gebirge}.")
    

def sortierer_höhe(eintrag):
    print(eintrag)
    return eintrag[1]

""" sortiere nach höhe"""
for berg, höhe, gebirge in sorted(berge, key=sortierer_höhe):
    print(f"{berg} ist {höhe} m hoch und im Gebirge {gebirge}.")

def sortierer_gebirge(eintrag):
    return [eintrag[2], eintrag[0]]

""" sortiere nach gebirge"""
for berg, höhe, gebirge in sorted(berge, key=sortierer_gebirge, reverse=False):
    print(f"{berg} ist {höhe} m hoch und im Gebirge {gebirge}.")

