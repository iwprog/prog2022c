"""
Stundenwiederholung vom 5. April 2022
"""
f = open("hallo.txt", "w", encoding="utf8")
f.write("Guten Morgen\n")
f.write("und einen schönen Tag\n")
f.write("und eine weitere Zeile\n")
f.close()

with open("hallo.txt", "w", encoding="utf8") as f:
    f.write("Guten Morgen\n")
    f.write("und einen schönen Tag\n")
    f.write("und eine weitere Zeile")

""" erste nicht eingerückte Zeile => hier wird die Datei geschlossen... """
