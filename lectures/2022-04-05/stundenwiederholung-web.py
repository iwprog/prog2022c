from urllib.request import urlopen
from inscriptis import get_text


with urlopen("https://meteo.search.ch") as source:
  web_content = source.read().decode("utf8")
  text_content = get_text(web_content)
  print(text_content)
  
