# Dictionnary mit CH Städten + Einwohnerzahlen
# Zürich hat 370000 Einwohner
# Genf hat 190000 Einwohner

cities={"Frauenfeld":30, "Chur": 39, "Winterthur":50, "Zürich":80,
        "Diessenhofen":10}

for i in sorted(cities):
    print(i, "hat", cities[i], "Einwohner")

sortedDict = list(sorted(cities.items()))
print(sortedDict)

liste = []
for city in cities:
    liste.append([cities[city], city])

print(sorted(liste))

for einwohner, stadt in sorted(liste):
    print(stadt, "hat", einwohner)