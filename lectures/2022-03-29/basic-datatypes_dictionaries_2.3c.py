preisliste = {"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2}

print("Ausgabe all jener ohne 'en'")
for lebensmittel in preisliste:
    if lebensmittel[-2:] != "en":
        print(lebensmittel, "-->", preisliste[lebensmittel])
        
print("Entfernen all jener, die auf 'en' enden - Variante 1")
preisliste2 = dict(preisliste)
for lebensmittel in preisliste2:
    if lebensmittel[-2:] == "en":
        del preisliste[lebensmittel]

print(preisliste)

preisliste = {"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2}
print("Entfernen all jener, die auf 'en' enden - Variante 2")
for lebensmittel in list(preisliste):
    if lebensmittel[-2:] == "en":
        del preisliste[lebensmittel]
print(preisliste)