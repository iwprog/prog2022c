
# a. Erstellen Sie ein Dictionary Preisliste, welches folgende Produkte mit
# den zugehörigen Preisen speichert: Brot → 3.2, Milch → 2.1, Orangen
# → 3.75, Tomaten → 2.2.

preisliste = {
"Brot" : 3.2,
"Milch": 2.1,
"Orangen": 3.75,
"Tomaten": 2.2,
 }
#b
preisliste["Milch"] = 2.05

#c
preisliste.pop("Brot") #alternative zu del
del preisliste["Brot"]

#d
preisliste["Tee"]=4.2
preisliste["Peanuts"]=3.9
preisliste["Ketchup"]=2.1
