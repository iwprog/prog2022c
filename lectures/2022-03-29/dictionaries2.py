
l_input = "Der Tag begann sehr gut! Der Morgen war scḧon."

sortierliste = {}
def wörter_zählen(l_input):
    dictionary = {}
    text_kleingeschrieben = l_input.lower()
    worte = text_kleingeschrieben.split()

    for eintrag in worte:
        if eintrag in dictionary:
            dictionary[eintrag] = dictionary[eintrag] + 1
        else:
            dictionary[eintrag] = 1
            
    return dictionary
gezählte_worte = wörter_zählen(l_input)
print(gezählte_worte)
