"""
Stundenwiederholung vom 29. März 2022
"""

""" Teil 1 """
""" Teil 1 """
dict_namen={"Xenia": 12, "Ana": 26,"Manuel": 17, "Susanne": 66}
alter_liste = [26, 17, 66]

print(dict_namen["Ana"])

dict_namen["Ana"] = 27
dict_namen["Ana"] = dict_namen["Ana"] + 1

for n in dict_namen:
  print(n,"ist",dict_namen[n],"Jahre alt.")

print("\n\n---Sortiert:")
for n in sorted(dict_namen):
  print(n,"ist",dict_namen[n],"Jahre alt.")

""" Prüfen Sie, ob es jemanden gibt, der/die
     (a) 17 Jahre alt ist
     (b) 22 Jahre alt ist.
"""

for value in dict_namen.values():
  if value == 17:
    print(value)

print(17 in dict_namen.values())
print(22 in dict_namen.values())

""" Beispiel 2: Erstellen eines Dictionaries """

d={}
while True:
    name = input("name:")
    if name == "x":
        break
    alter = input("Alter: ")
    if alter == "x":
        break
    else:
        d[name] = alter

print(d)

d={}
while True:
    name = input("name:")
    if name == "x":
        break
    alter = input("Alter: ")
    if alter == "x":
        break
    
    d[name] = alter

print(d)