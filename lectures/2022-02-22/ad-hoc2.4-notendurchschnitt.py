
def notenschnitt(name, note1, note2, note3, note4, note5, note6):
    schnitt=round((note1 + note2 + note3 + note4 + note5 + note6) / 6, 2)
    return name.split(" ")[1]+", Notenschnitt: " + str(schnitt)

print(notenschnitt("Max Muster", 4, 5.5, 6, 4, 5, 4.5))