# Stundenwiederholung vom 22. Februar 2022

# - Syntax
# - Semantik
# - Pragmatik
# - Paradigma
#     - objektorientiert
#     - funktionale
#     - imperativ
#     - descriptiv
    
print("hallo "*10)

# Variablen/Funktionen in Python
# - kleingeschrieben
# - mehrere wörter werden mit "_" getrennt
# - name, task_liste, ...
#
# KONSTANTE 
# - uppercase
#
# Klassen
# - Gross- und Zusammengeschrieben
# - Bsp: Obst, ObstTeller, ...

print("hallo "*10)

#name = input("Ihr Name? ")
#print(("Hallo " + name + "! ") * 10)

# extrahieren sie den term gesellschaft aus untigen string
item = "Donaudampfschiffsfahrtsgesellschaftskapitän"
print(item[23:35])

preis = 200
print("MWSt: ", 200*0.077)

# Was funktioniert?
# print("Betrag: " + preis) # liefert einen fehler
print("Betrag: ", preis)

# typen von variablen

text = "Ein Text 12+4" 
zahl = 77
kommazahl = 77.
kommazahl2 = 6.022E23

