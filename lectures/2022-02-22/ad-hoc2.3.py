from turtle import *

def jump(winkel, strecke):
    penup()
    left(winkel)
    forward(strecke)
    right(winkel)
    pendown()
    
def dreieck():
    begin_fill()
    forward(100)
    left(120)
    forward(100)
    left(120)
    forward(100)
    left(120)
    end_fill()
    
dreieck()
jump(90,200)
dreieck()