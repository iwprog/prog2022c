def f1():
    print("Funktion f1")
    
print("Guten Tag!")

def f2_zahl(zahl):
    print("Funktion f2: ", zahl)
    f1()
    
def f3_summe(a, b):
    summe = a + b
    f2_zahl(a * b)
    print("Funktion f3: ", summe)
    
#f1()
#f2_zahl(12)
#f3_summe(2, 7)
f1()