"""
1. Schreiben Sie ein Funktion "dreieck", welche ein Dreieck mittels _Schleife_ zeichnet
2. Fragen Sie den Benutzer, wie viele Dreiecke gezeichnet werden sollen.
   - Wie viele Dreiecke soll ich zeichnen?
-- 
3. Schreiben Sie zusätzlich die Funktionen "viereck" und "kreis".
4. Bitten Sie die Benutzerin um die Eingabe eines Musters:
   zum Beispiel: "#od" -> zeichnet: Viereck, Kreis, Dreieck
                 "##oo" -> zeichnet: Viereck, Viereck, Kreis, Kreis
"""