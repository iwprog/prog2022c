"""
Schreiben Sie ein Programm, dass Zahlen zwischen 1 und 3 in einer Schleife entgegenimmt
und
- den entsprechenden Monatsnamen ausgibt
- 99 = exit

Beispiel:
=========
Zahl? 3
März
Zahl? 2
Februar
Zahl? 99

bis 10:41
"""
zahl = 0
while zahl != 99:
    zahl = input("Bitte eine Zahl eingeben: ")
    if zahl.isdigit():
        zahl = int(zahl)
        if zahl  == 1:
            print ("Januar")
        elif zahl  == 2:
            print ("Februar")
        elif zahl  == 3:
            print ("März")
