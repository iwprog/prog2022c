"""
# Aufgabe:
# - Ihre Programm soll nach Zahlen Fragen
# - diese sollen summiert werden, bis der Benutzer "quit" eingibt.
#
# Beispiel
# - Zahl (oder quit): 5
# - Zahl (oder quit): 4
# - Zahl (oder quit): 15
# - Zahl (oder quit): quit
# - Summe: 24
#
# bis 9:37
"""

summe = 0
maxi = 0
zahl = ""
while zahl != "quit":
    zahl = input("Zahl oder <<quit>> eingeben: ")
    if zahl.isdigit():
        zahl = int(zahl)
        summe = summe + zahl
        if zahl > maxi:
            maxi = zahl
        

print("Summe:", summe)
print("Maximum:", maxi)