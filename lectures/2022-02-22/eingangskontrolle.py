# (a) Fragen Sie eine Person nach deren Alter
#     - mindesten 18 Jahre alt => darf eintreten
#     - nicht 18 Jahre alt => darf nicht eintreten
# (b) Fragen Sie nach dem Geburtsjahr
# 

from turtle import *

# alter = numinput("Abfrage", "Wie alt sind Sie?")
alter = input("Wie alt sind Sie?")
# mache aus dem string "alter" die zahl "alter"
alter = int(alter)
if alter > 17:
    print("Darf eintreten")
else:
    print("Darf nicht eintreten")
    
# konversion von text zu string - drei möglichkeiten
# 1. direkt im input
alter = int(input("Wie alt sind Sie"))

# 2. in einem eigenen schritt
alter = input("Wie alt sind Sie?")
alter = int(alter)

# 3. im vergleich
alter = input("Wie alt sind Sie?")
if int(alter) > 17:
    print("Darf eintreten...")
else:
    print("Darf nicht eintreten...")
    