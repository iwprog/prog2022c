from turtle import *

# Globale Variablen
seitenlaenge = 10
def dreieck(x, y):
    print(x, y)
    goto(x,y)
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    left(120)
    forward(seitenlaenge)
    
reset()
onscreenclick(dreieck)
done()
