# Liste mit allen Jahreszeiten 
jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]

print(jahreszeiten)

# Frühling aus Liste entfernen
jahreszeiten.remove("Frühling")
# del jahreszeiten[0]

print(jahreszeiten)

# Langas der Liste hinzufügen
jahreszeiten.append("Langas")
jahreszeiten.insert(0, "Langas")

print(jahreszeiten)

##

namensliste = []
name = ""

while name != "x":
    name = input("Namen hinzufügen: ")
    namensliste.append(name)

namensliste.remove("x")


##
while True:
    name = input("Namen hinzufügen: ")
    if name == 'x':
        break
    namensliste.append(name)
