"""
I. Liste mit
 (a) name
 (b) preis
 (c) menge
 (d) shop
 
II. Sortierte Ausgabe nach
 (a) name
 (b) preis
 (c) shop
 
"""

liste=[]

while True:
    produkt=input("Artikel? ")
    if produkt=="x":
        break
    else:
        preis = float(input("Wie viel kostet der Artikel? "))
        menge = float(input("Wie viel wollen Sie kaufen? "))
        shop = input("Wo wollen Sie den Artikel kaufen? ")
        artikel = produkt, preis, menge, shop
        liste.append(artikel)

liste=sorted(liste)        
summe=0
print(30*"-")

## Variante A

    
print(30*"-","\nGesamtpreis:",summe,"CHF")
for name, preis, menge, shop in liste:
    print(menge, "x", name,"--- Preis:", preis,"CHF", "--", shop)
    summe=summe+preis*menge

print(30*"-","\nGesamtpreis:",summe,"CHF")
