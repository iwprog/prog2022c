def replace(text, position, einfügetext):
    text = text[:position] + einfügetext + text[position:]
    #text[:position] gibt "Das ist eine einfache" aus
    #einfügetext --> fügt unser Einfügewort "Python" ein
    #text[position] gibt "Sequenz." aus
    
    return text

print(replace("Das ist eine einfache Sequenz.", 21, " Python"))
#              0123456789012345678901
#              0          1         2
