from turtle import *

def screen():
    print("============================")
    print("Programmübersicht:")
    print("1 ... Preis für eine Fahrkarte berechnen")
    print("2 ... Eintritt für das Schwimbad berechnen")
    print("3 ... Mahngebühr für die Bibliothek berechnen")
    print("")
    print("0 ... Programm beenden")
    print("============================")
    


def fahrkarte(a, b):
    if a < 6:
        x=0
    elif a < 16:
        x=(2+(b*0.25))/2
    else:
        x=2+(b*0.25)
    print("Preis für Fahrkarte berechnen")
    print("-------------------------------")
    print("Bitte geben sie Ihr Alter ein:", a)
    print("Wie viele km wollen Sie reisen?", b)
    print("Die Fahrkarte kostet:", x, "CHF")

def schwimmbad(a, b, c):
    if c.lower() == "kurzzeitkarte":
        x = 5
    elif c.lower() == "nachmittagskarte":
        x= 6
    elif c.lower() == "tageskarte":
        x = 10
    else:
        print("Ungültige Eingabe")
    if b :
        x=x-(x*0.10)
    elif b :
        pass
    else:
        print("Ungültige Eingabe")
    
    if a < 7:
        x = 0
    elif a < 13:
        x= x*0.5
    print("Eintritt für Schwimmbad berechnen")
    print("-------------------------------")
    print("Bitte geben sie Ihr Alter ein:", a)
    print("Eintrittspreis für Schwimbad", x, "CHF")
    
def bib(a, b, c):
    if a == 1:
        x=2*b
    elif a == 2:
        x=4*b
    elif a == 3:
        x=6*b
    else:
        print("Ungültige Eingabe")
    
    if c.lower() == "post" or a == 3:
        x=x+2
    else:
        pass
    
    print("Bibliotheks Mahngebühr berechnen")
    print("-------------------------------")
    print("Nach der "+ str(a)+ ". Mahnung")
    print("Mahngebühr: "+ str(x)+ " CHF")
    print("Die Mahngebühren wurden übermittelt über "+ str(c))
 
  
while True:
    screen()
    nummer = int(input("Welche Nummer...."))
    if nummer == 1:
        alter = numinput("Alter eingeben", "Wie alt bist du?")
        strecke = numinput("Strecke eingeben", "WIe weit fährst du?")
        fahrkarte(alter, strecke)
    elif nummer == 2:
        alter= numinput("Alter eingeben", "Wie alt bist du?")
        ermassigung= textinput("Hast du eine Ermässigungskarte", "Ja / Nein")
        if ermassigung.lower() == "ja":
            ermassigung= True
        elif ermassigung.lower() == "nein":
            ermassigung= False
        else:
            print("Ungültige Eingabe")
        karte = textinput("Welche Karte müchtest du kaufen?", "Kurzzeitkarte, Nachmittagskarte, Tageskarte ")
        schwimmbad(alter, ermassigung, karte)
    elif nummer == 3:
        mahnung = numinput("Anzahl an Mahnungen", "Die wievielte Mahnung ist es?")
        medien = numinput("Anzahl der Medien", "Wie viele Medien sind ausgeliehen?")
        post= textinput("Wie wurde die Mahnung versandt?", "Post / Mail")
        bib(mahnung, medien, post)
    elif nummer > 3:
        print("Ungültige Option")
    elif nummer == 0:
        break

