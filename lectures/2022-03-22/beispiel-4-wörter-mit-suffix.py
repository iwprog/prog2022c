""" Pause bis 9:21 """

def suffix(liste, ende):
    ergebnis = []
    for wort in liste:
        if wort[-len(ende):] == ende:
            ergebnis.append(wort)
    return ergebnis

meine_liste = ["Gesundheit", "Wanderung", "Heiterkeit", "Gewandtheit", "Lustig"]
print(suffix(meine_liste, "eit"))