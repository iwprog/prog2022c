#
# Stundenwiederholung vom 22. März 2022
#

"""Variable versus Konstante/String"""
eingabe = input("Eingabe: ")
if eingabe == "Chur":
    print("Hier sind wir....")
elif eingabe == "Zürich":
    print("Zürich")
    

""" Funktionen _mit_ Parameter """
def zeichne_haus(hoehe):
    forward(hoehe)
    # ...

zeichne_haus(100)
zeichne_haus(200)
zeichne_haus(300)

""" int versus float für range! """
anzahl = numinput("Eingabe", "Anzahl")
for x in range(int(anzahl)):
    print(x)
    
""" Tarnen und Täuschen... """

def berechne_ergebnis(a, b):
    ergebnis = a * b
    return ergebnis

ergebnis = berechne_ergebnis(1, 2)
print("Ergebnis:", ergebnis)
if ergebnis > 1:
    print("Grösser 1")
else:
    print("Kleiner 1")


""" In den Sonnenuntergang reiten..."""

def berechne_ergebnis(a, b):
    ergebnis = a * b
    return ergebnis

    # alles was nach return kommt, wird ignoriert
    if ergebnis > 1:
        print("Grösser")
    else:
        print("Kleiner")
        
berechne_ergebnis(1, 2)


""" Nicht in den Sonnenuntergang reiten...."""

def berechne_ergebnis(a,b):
    eregebnis = a * b
    # return ergebnis sorgt erst dafür, dass ein ergebnis
    # ausgegeben wird.
    
print(berechne_ergebnis(1,2))
