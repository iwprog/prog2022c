from random import randint
def aufgabe():
    l1= []
    l2= []
    i=1
    while i < 25:
        l2.insert(0, i)                 # l2: tag + überraschung
        l2.insert(1, randint(1, 5))
        i=i+1
        l1=l1 + [l2]                    # alle tage + überraschungen
        l2= []

    a= int(input("Welche Kalendertür wollen sie öffnen? (oder x für Exit)"))
    if a < 1 or a > 24:
        print("Falsche Antwort")
    else:
        antwort= int(l1[a-1][1])
        if antwort == 1:
            print("Samiklaus")
        elif antwort == 2:
            print("Christbaum")
        elif antwort == 3:
            print("Weihnachtskugel")
        elif antwort == 4:
            print("Stiefel")
        else:
            print("Schneeball")
    
while True:
    aufgabe()
    if input() == "x":
        break
