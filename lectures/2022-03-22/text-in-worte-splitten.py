"""
Wörter zählen (Stufe 1: bis 10:35)

Aufgabe: Schreiben Sie eine Funktion, welche
 (a) einen beliebigen Text entgegennimmt und
 (b) ein dictionary zurückgibt, welches angibt welche Wörter
     und wie oft diese im Text vorkommen.
 (c) geben Sie die Liste der Wörter sortiert aus.
 (d***) geben Sie die Wörter nach deren _Häufigkeit_ sortiert aus.
     
Schritte:
 (a) Text in Wörter splitten
 (b) die Liste der Wörter ausgeben
 (c) Wörter in ein Dictionary schreiben und mitzählen wie oft
     diese bereits vorgekommen sind.
"""

text = "mein huhn legt ein ei und ein zweites"

def wörter_zählen2(text):
    """ Idee gut - aber funktioniert in diesem
        Kontext nicht immer.
    """
    worte = text.split()
    dictionary = {}    
    for wort in worte:
        dictionary[wort] = text.count(wort)
            
    return dictionary


def wörter_zählen(text):
    dictionary = {}
    worte = text.split()

    for eintrag in worte:
        if eintrag in dictionary:
            dictionary[eintrag] = dictionary[eintrag] + 1
        else:
            dictionary[eintrag] = 1
            
    return dictionary

# print(dictionary)
gezählte_worte = wörter_zählen(text)
for element in sorted(gezählte_worte):
    print(element, "-->", gezählte_worte[element])

""" sortierung nach der anzahl """
sortierliste = []
for element in gezählte_worte:
    item = gezählte_worte[element], element
    sortierliste.append(item)

sortierliste.sort()
print(sortierliste)

for anzahl, wort in sortierliste:
    print(wort, "-->", anzahl)


