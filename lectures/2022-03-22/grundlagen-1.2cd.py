liste1 = ["Frühling", "Sommer", "Herbst", "Winter"]

# (c)
for i in liste1:
    print(i, "-", i[-2:])
    
print("Zeile\nund die zweite Zeile")

# (d)
for i in liste1:
    a = i[-2:]
    if a == "er":
        print(i)
        
# alternative
for i in liste1:
    if a.endswith("er"):
        print(i)
