cities = {"Zürich":370000, "Genf":190000, "Basel":170000,
          "Bern":130000}

print(list(sorted(cities)))
for stadt in sorted(cities):
    print(stadt, "hat", cities[stadt], "Einwohner.")

# Variante 2

print(list(cities.items()))
for stadt, einwohner in sorted(cities.items()):
    print(stadt, "hat", einwohner, "Einwohner.")