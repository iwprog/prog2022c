from random import choice
advent = []
zufallsliste = ["Samiklaus","Christbaum","Weihnachtskugel","Stiefel","Schneeball"]

for i in range(24):
    zufallselement = choice(zufallsliste)
    advent.append(zufallselement)
    
while True:
    eingabe = input("Welche Kalendertür wollen Sie öffnen? (oder x für Exit):")
    if eingabe == "x":
        break
    print(advent[int(eingabe)-1])
    advent[int(eingabe)-1] = "leer"
    
