"""
https://www.worldometers.info/world-population/switzerland-population/
https://weichselbraun.net/tmp/swiss.html
"""
from csv import writer, reader
from inscriptis import get_text
from urllib.request import urlopen

firstrun = False
if firstrun:
    with urlopen("https://weichselbraun.net/tmp/swiss.html") as f:
        file_content = f.read().decode("utf8")
        text_content=get_text(file_content)
      
    with open("text.txt", "w", encoding="utf8") as j:
        j.write(text_content)

"""Datei existiert bereits => einlesen"""
with open("text.txt", encoding = "utf8") as f:
    file_content = f.read()
    print(file_content)

"""Erstelle eine Liste mit den Zeilen"""
zeilen = file_content.split("\n")
with open("bevölkerung.csv", "w", encoding="utf8") as f:
    csv_writer = writer(f, delimiter=";")
    for zeile in zeilen:
        columns = zeile.split()
        csv_writer.writerow(columns)
