""" weather.json einlesen"""
from json import loads, dumps

with open("weather.json",encoding="utf8") as f:
  t = f.read()
  weather= loads(t)
  print(weather)
  
  for stadt in weather:
      print(stadt+" Temperatur:", weather[stadt]["temperatur"])
      print(stadt+" Humidity  :", weather[stadt]["humidity"])
  
  """Ausgabe: Variante II"""
  for stadt, measurements in weather.items():
      print(f"{stadt}: Temperatur: {measurements['temperatur']}, Hum: {measurements['humidity']}")
      
  
