"""
Stundenwiederholung vom 12. April 2022
"""


"""Schreiben einer Liste in eine CSV Datei"""
from csv import writer

meine_daten = [
  ["Ana", "+41 76 123 32 21"],
  ["Joe", "+41 81 286 37 72"],
  ["Zoe", "+41 79 231 12 32"]
]

with open("Telefon.csv", "w", newline="\n", encoding="utf8") as f:
    csv_writer=writer(f, delimiter=";")
    for row in meine_daten:
        csv_writer.writerow(row)