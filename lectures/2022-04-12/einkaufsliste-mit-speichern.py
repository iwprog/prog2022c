import sys
import csv
from math import fsum

Einkaufsliste = []


def aktuelleListe():
    print("""Aktuelle Einkaufsliste
----------------------
    """)
    for a in Einkaufsliste:
        print(a["Menge"], "x", a["Artikel"], "---", a["Preis"])
    print()
        

#funktion Artikel hinzufügen
def artikelhinzufuegen():
    artikel = input("Artikelname oder x: ")
    if artikel == "x":
        return
    preis = float(input("Preis: "))
    menge = int(input("Menge: "))
    temp = {
        "Artikel": artikel,
        "Preis": preis,
        "Menge": menge
    }
    Einkaufsliste.append(temp)



#Funktion Artikel löschen
def artikelloeschen():
    for position, a in enumerate(Einkaufsliste):
        print(position, "|", a["Menge"], "x", a["Artikel"], "---", a["Preis"])
    artikel = int(input("Welcher Artikel soll gelöscht werden (Index eingeben): "))
    Einkaufsliste.pop(artikel)


#Funktion suchen
def artikelsuchen():
    suchstring = input("Suchstring eingeben: ")
    for element in Einkaufsliste:
        if suchstring.lower() in element["Artikel"].lower():
            print(element["Menge"], "x", element["Artikel"], "---", element["Preis"])

#Funktion einfkaufsliste leeren
def listeloeschen():
    wahl = input("Einkaufsliste leeren?(Y/N) ")
    if wahl == "Y":
        Einkaufsliste.clear()
        print("Die Liste wurde geleert")
        #Alternativ --> del Einkaufsliste[:]
    if wahl == "N":
        return

#Funktion in csv
def csvexport():
    with open("einkaufsliste.csv", "w", newline = "", encoding="utf8") as f:
        """Sophisticated version"""
        csv_writer = csv.DictWriter(f, delimiter=";", fieldnames = ["Artikel", "Preis", "Menge"])
        csv_writer.writeheader()
        csv_writer.writerows(Einkaufsliste)
        
        """Alternative version using our skills."""
        csv_writer = writer(f, delimiter=";")
        csv_writer.writerow(["Artikel", "Preis", "Menge"])
        for artikel in Einkaufsliste:
            csv_writer.writerow([artikel["Artikel"], artikel["Preis"], artikel["Menge"]])

#Funktion gesamtbetrag einkauf berechen
def gesamtpreis():
    tempList = []
    for element in Einkaufsliste:
        tempList.append(element["Preis"])
    
    s = fsum(tempList)
    print("Der Gesamtpreis beträgt:", s, "CHF.")
    
#exit
print(""" Auswahl:
        (1) artikel hinzufügen
        (2) artikel löschen
        (3) artikel suchen
        (4) einkaufsliste leeren
        (5) einkaufsliste im csv format exportieren
        (6) gesamtbetrag einkauf berechnen
        (0) exit
""")

while True:
    aktuelleListe()
    userinput = int(input("Geben Sie eine Zahl ein: "))
    if userinput == 1:
        artikelhinzufuegen()
    if userinput == 2:
        artikelloeschen()
    if userinput == 3:
        artikelsuchen()
    if userinput == 4:
        listeloeschen()
    if userinput == 5:
        csvexport()
    if userinput == 6:
        gesamtpreis()
    if userinput == 0:
        break
        sys.exit(0)
