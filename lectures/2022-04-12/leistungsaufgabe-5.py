from urllib.request import urlopen
from csv import writer
from csv import reader
from inscriptis import get_text

link = input("Fügen Sie hier Ihren Link ein: ")

if "https://" or "http://" in link:
    with urlopen(link) as f:
        file_content = f.read().decode("utf8")
        text = get_text(file_content)
        wortliste=text.split()
else:
    with open(link,encoding="utf8") as f:
        csv_reader=reader(f, delimiter=";")
        wortliste=[]
        file_content=""
        for row in csv_reader:
            for i in row:
                wortliste.append(i)
                file_content=file_content+i

""" Zählen der Wort """
wortzähler={}
for i in wortliste:
    if i in wortzähler:
        wortzähler[i]=wortzähler[i]+1
    else:
        wortzähler[i]=1
        
""" Zählen der Buchstaben """
buchstabenzähler={}
for i in text:
    if i.lower() in buchstabenzähler:
        buchstabenzähler[i.lower()]=buchstabenzähler[i.lower()]+1
    else:
        buchstabenzähler[i.lower()]=1

with open("wort.csv","w",encoding="utf8") as a:
    csv_writer=writer(a,delimiter=";")
    for wort in sorted(wortzähler):
        csv_writer.writerow([wort,wortzähler[wort]])
        
with open("buchstabe.csv","w",encoding="utf8") as b:
    csv_writer=writer(b,delimiter=";")
    for buchstabe in sorted(buchstabenzähler):
        csv_writer.writerow([buchstabe,buchstabenzähler[buchstabe]])
    