"""Datei: 'telefon.csv' """


"""Datei: 'Telefon.csv' """
from csv import writer, reader

"""Option 1"""
with open('Telefon.csv', encoding = "utf8") as f:
  csv_reader= reader(f, delimiter=";")

  for row in csv_reader:
      print("Name:", row[0], "Telefonnummer:", row[1])
      
"""Option 2"""
with open('Telefon.csv', encoding = "utf8") as f:
  csv_reader= reader(f, delimiter=";")

  for name, phone in csv_reader:
      print("Name:", name, "Telefonnummer:", phone)
      print(f"{name}: {phone}")
