from random import choice
wörter = ["Banane","Ukulele","Schokolade","Ananas"]

characters_to_swap = {'a':'e', 'e':'i', 'i':'o', 'o':'u', 'u':'a'}
wort = choice(wörter)

for character in wort:
    if character in characters_to_swap:
        print(characters_to_swap[character], end='')
    else:
        print(character, end='')