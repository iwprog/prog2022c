from json import dumps, loads

weather = {'chur': {'temperatur': 12.5,
                  'humidity': 34,
                  'sun': 5,
                  'wind_speed': 12.5,
                  'wind_direction': 'S'
                  },
          'zurich': {'temperatur': 14.5,
                  'humidity': 54,
                  'sun': 1,
                  'wind_speed': 2.5,
                  'wind_direction': 'N'
                  }
         }

json_string = dumps(weather)
with open("weather.json", "w", encoding="utf8") as f:
  f.write(json_string)