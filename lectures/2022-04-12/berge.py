from json import loads, dumps

berge = {}

try:
    with open("berge.json", encoding="utf8") as f:
        json_string = f.read()
        berge = loads(json_string)
except FileNotFoundError:
    print("Die Datei exisitert noch nicht, nur das was jetzt eingegeben wird, wird angezeigt")

i = len(berge) + 1
while True:
    berg = input(str(i) + ". Berg: ")
    if berg == "x":
        break
    höhe = input(str(i) + ". Höhe: ")
    berge[berg] = höhe
    i = i+1
    
   
for berg, höhe in sorted(berge.items()): 
    ft = round(int(höhe)*3.28)
    print(f"{berg} ist {höhe} m ({ft} ft) hoch.") #das ist ein f string


with open("berge.json", "w", encoding="utf8") as f:
    json_string = dumps(berge)
    f.write(json_string)

