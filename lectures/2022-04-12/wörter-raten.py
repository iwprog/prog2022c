"""
Wörter raten
- vertauschen Sie die Vokale in Wörter zufällig und lassen Sie im Anschluss
  den Benutzer das Wort raten.
  
  
Bsp:
  Birnen < Bornin
  Wasserschlacht < Wusserschlucht
  Wein < Waun
  
Frage:
  mit welchem Beispiel ist dieses Beispiel verwandt?
  - Wörter schwärzen
  - ICAO Buchstabieren
  
Funktioniert:
  a > e
  e > i
  o > u
  u > o
  
Funktioniert nicht
  a > e
  o > e
  u > i
  e > i
"""
from random import choice

def scramble_word(wort):
    fragewort = ""
    for i in wort:
        if "a" in i:
            fragewort = fragewort + "e"
        elif "e" in i:
            fragewort = fragewort + "i"
        elif "i" in i:
            fragewort = fragewort + "o"
        elif "o" in i:
            fragewort = fragewort + "u"
        elif "u" in i:
            fragewort = fragewort + "a"
        else:
            fragewort = fragewort + i  
    return fragewort    

wortliste = ["Baum", "Busch", "Strauch", "Wald", "Wiese"]
zufallswort = choice(wortliste)

print("Was versteckt sich hinter", scramble_word(zufallswort), "?")
antwort = input("Deine Antwort? ")

if antwort == zufallswort:
    print("Richtig!")
else:
    print("Leider Falsch, die richtige Antwort lautet", zufallswort)
