"""
Aufgabe:
  1. Erweitern Sie das Beispiel, sodass dieses auch den Mittelwert berechnet
  2. Sodass ein Benutzer Zahlen in einer Schleife eingeben kann und für diese
     max/min/avg/sum berechnet werden.

Zahl? 12
Zahl? 24
Zahl? q

Summe: 36
Minimum: 12
Maximum: 24
Durchschnitt: 18

bis 9:01
"""

maximum = None
minimum = None
summe = 0
for i in 12, 23, 5, 56, 18, 9:
    summe=summe+i
    if maximum == None or i  > maximum:
        maximum = i
    if minimum == None or i < minimum:
        minimum = i 
              
print("Durchschnitt = ", summe/6)
print("Maximum =", maximum)
print("Minimum =", minimum)
