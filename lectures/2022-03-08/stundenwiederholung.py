# Stundenwiederholung vom 8. März 2022

# Arten von Schleifen - for / while

for i in range(1, 5):
    print(i)
    print(i**2)
    print(i**3)
print("Ich werde nicht mehr wiederholt")

while True:
    print("Ich werde wiederholt solange es stimmt...")
    