maximum = None
minimum = None 
summe = 0
durchlauf = 0 

"""
Zwei neue Befehle für Schleifen

 1. break ... bricht eine Schleife sofort ab
 2. continue ... startet sofort die nächste Wiederholung
"""

while True:
    zahl = input("Bitte eine Zahl oder q eingeben ")
    if zahl == 'q':
        break
    
    if not zahl.isdigit():
        continue
        
    zahl=int(zahl)
    summe = summe + zahl
    durchlauf = durchlauf + 1
    if maximum == None or zahl > maximum:
        maximum = zahl
    if minimum == None or zahl < minimum:
        minimum = zahl
    
    
""" round(z, n): rundet eine zahl "z" auf "n" stellen """              
print ("Durchschnitt = ", round(maximum/durchlauf, 2))
print ("Maximum =", maximum)
print("Minimum =", minimum)
print("Summe = ", summe)
