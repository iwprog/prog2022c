from turtle import *

pensize(2)
speed(10)

penup()
back(800)
pendown()

def dreieck(farbe_dreieck):
    pencolor(farbe_dreieck)
    fillcolor(farbe_dreieck)
    begin_fill()
    fd(100)
    lt(120)
    fd(100)
    lt(120)
    fd(100)
    lt(120)
    end_fill()
    penup()
    fd(120)
    pendown()
    
def quadrat(farbe_quadrat):
    pencolor(farbe_quadrat)
    fillcolor(farbe_quadrat)
    begin_fill()
    fd(100)
    lt(90)
    fd(100)
    lt(90)
    fd(100)
    lt(90)
    fd(100)
    lt(90)
    end_fill()
    penup()
    fd(120)
    pendown()
    
def kreis(farbe_kreis):
    penup()
    fd(50)
    pendown()
    pencolor(farbe_kreis)
    fillcolor(farbe_kreis)
    begin_fill()
    circle(50)
    end_fill()
    penup()
    fd(60)
    pendown()
    

farbe_kreis = "red"
farbe_dreieck = "green"
farbe_quadrat = "orange"
form = textinput("Formauswahl","Welche Formen sollen gezeichnet werden?")

for symbol in form:
    if symbol == "d":
        #farbe_dreieck = textinput("Farbauswahl", "Welche Farbe soll das Dreieck haben?")
        dreieck(farbe_dreieck)      
    elif symbol == "#":
        #farbe_quadrat = textinput("Farbauswahl", "Welche Farbe soll das Quadrat haben?")
        quadrat(farbe_quadrat)    
    elif symbol == "o":
        #farbe_kreis = textinput("Farbauswahl", "Welche Farbe soll der Kreis haben?")
        kreis(farbe_kreis)

exitonclick()