from turtle import *
speed(10)
pensize(5)
farbe="red"
for i in range(5):
    pencolor(farbe)
    forward(200)
    left(72)
    if farbe=="red":
        farbe="blue"
    elif farbe=="blue":
        farbe="green"
    elif farbe=="green":
        farbe="yellow"
    elif farbe=="yellow":
        farbe="orange"
exitonclick()