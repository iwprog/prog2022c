from turtle import *

def d():
    for i in range(3):
        fd(100)
        lt(120)
    fd(100)
    
def o ():
    penup()
    fd(100)
    lt(90)
    fd(50)
    pendown()
    circle(50)
    penup()
    lt(180)
    fd(50)
    lt(90)
    pendown()
    
def sq():
    for i in range(4):
        fd(100)
        lt(90)
    fd(100)

sq(), sq(), d(), d(), o(), o()